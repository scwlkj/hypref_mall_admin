# 项目介绍

Hyperf Mall项目 路由入口，基于HTTP服务。前端和后台路由实现。

要求：

1、前端参数必须校验

2、前后端控制器目录单独分开

3、开发时注意继承基类

4、建议在当前服务中做一定的缓存

注意事项：

1、后台路由采用注解方式

2、C端路由改为文件形式（受中间件影响）

# 环境要求

 - PHP >= 7.3
 - Swoole PHP extension >= 4.5，and Disabled `Short Name`
 - OpenSSL PHP extension
 - JSON PHP extension
 - PDO PHP extension （If you need to use MySQL Client）
 - Redis PHP extension （If you need to use Redis Client）
 - Protobuf PHP extension （If you need to use gRPC Server of Client）
