<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

$consumers =  ['consumers' => []];

if(!function_exists('scanConfig')){
    function scanConfig($path,array $services){
        $base_config = [
            'protocol' => 'jsonrpc',
            'load_balancer' => 'random',
            'options' => [
                'connect_timeout' => 5.0,
                'recv_timeout' => 5.0,
                'settings' => [
                    'open_eof_split' => true,
                    'package_eof' => "\r\n",
                    // 'open_length_check' => true,
                    // 'package_length_type' => 'N',
                    // 'package_length_offset' => 0,
                    // 'package_body_offset' => 4,
                ],
                'pool' => [
                    'min_connections' => 1,
                    'max_connections' => 32,
                    'connect_timeout' => 10.0,
                    'wait_timeout' => 3.0,
                    'heartbeat' => -1,
                    'max_idle_time' => 60.0,
                ],
            ],
            //'registry' => [
               //'protocol' => 'consul',
               //'address' => env('CONSUL_ADDRESS', 'http://127.0.0.1:8500'),
           //],
        ];
        $nextPaths = scandir($path);

        foreach($nextPaths as $nextPath){
            $newPath = $path.'/'.$nextPath;
            if(is_dir($newPath)){
                if($nextPath!='.' && $nextPath != '..'){
                    $services = scanConfig($newPath,$services);
                }
            }else{
                $service = require $newPath;
                $serviceName = $service['name'] ?? '';
                if(!empty($serviceName)){
                    $services[] = array_merge($service, $base_config);
                }
            }
        }
        return $services;
    }
}

$services = scanConfig(BASE_PATH.'/config/services',[]);

if(env('APP_ENV') =='local'){
    foreach ($services as &$service){
        if(isset($service['registry'])){
            unset($service['registry']);
        }
    }
}
$consumers['consumers'] = $services;
return $consumers;
