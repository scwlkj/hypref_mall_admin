<?php
/**
 * This file is part of Hyperf Mall.
 * 
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

declare(strict_types=1);

return [
    'name' => 'Activity::CouponService',
    'service' => \App\Service\Activity\Interfaces\CouponServiceInterfaces::class,
    'nodes' => [
        ['host' => env('ACTIVITY_RPC_HOST', '127.0.0.1'), 'port' => (int)env('ACTIVITY_RPC_PORT')],
    ],
];
