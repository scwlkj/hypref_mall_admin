<?php

return [
    'name' => 'Orders::CartService',
    'service' => \App\Service\Orders\Interfaces\CartServiceInterfaces::class,
    'nodes' => [
        ['host' => env('ORDERS_RPC_HOST', '127.0.0.1'), 'port' => (int)env('ORDERS_RPC_PORT')],
    ],
];
