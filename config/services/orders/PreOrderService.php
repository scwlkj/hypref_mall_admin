<?php

return [
    'name' => 'Orders::PreOrderService',
    'service' => \App\Service\Orders\Interfaces\PreOrderServiceInterfaces::class,
    'nodes' => [
        ['host' => env('ORDERS_RPC_HOST', '127.0.0.1'), 'port' => (int)env('ORDERS_RPC_PORT')],
    ],
];
