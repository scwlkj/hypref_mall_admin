<?php

return [
    'name' => 'Orders::OrderService',
    'service' => \App\Service\Orders\Interfaces\OrderServiceInterfaces::class,
    'nodes' => [
        ['host' => env('ORDERS_RPC_HOST', '127.0.0.1'), 'port' => (int)env('ORDERS_RPC_PORT')],
    ],
];
