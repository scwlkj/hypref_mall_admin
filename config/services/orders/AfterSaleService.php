<?php

return [
    'name' => 'Orders::AfterSaleService',
    'service' => \App\Service\Orders\Interfaces\AfterSaleServiceInterfaces::class,
    'nodes' => [
        ['host' => env('ORDERS_RPC_HOST', '127.0.0.1'), 'port' => (int)env('ORDERS_RPC_PORT')],
    ],
];
