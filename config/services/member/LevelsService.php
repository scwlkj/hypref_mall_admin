<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

return [
    'name' => 'Member::LevelsService',
    'service' => \App\Service\Member\Interfaces\LevelsServiceInterfaces::class,
    'nodes' => [
        ['host' => env('MEMBER_RPC_HOST','127.0.0.1'), 'port' => (int)env('MEMBER_RPC_PORT')],
    ],
];
