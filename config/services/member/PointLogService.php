<?php

return [
    'name' => 'Member::PointLogService',
    'service' => \App\Service\Member\Interfaces\PointLogServiceInterfaces::class,
    'nodes' => [
        ['host' => env('MEMBER_RPC_HOST', '127.0.0.1'), 'port' => (int)env('MEMBER_RPC_PORT')],
    ],
];
