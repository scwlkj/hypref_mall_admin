<?php

return [
    'name' => 'Member::SignService',
    'service' => \App\Service\Member\Interfaces\SignServiceInterfaces::class,
    'nodes' => [
        ['host' => env('MEMBER_RPC_HOST', '127.0.0.1'), 'port' => (int)env('MEMBER_RPC_PORT')],
    ],
];
