<?php
/**
 * This file is part of Hyperf Mall.
 * 
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

declare(strict_types=1);

return [
    'name' => 'Member::MemberPointService',
    'service' => \App\Service\Member\Interfaces\MemberPointServiceInterfaces::class,
    'nodes' => [
        ['host' => env('MEMBER_RPC_HOST', '127.0.0.1'), 'port' => (int)env('MEMBER_RPC_PORT')],
    ],
];
