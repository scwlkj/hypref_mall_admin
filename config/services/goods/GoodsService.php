<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

return [
    'name' => 'Goods::GoodsService',
    'service' => \App\Service\Goods\Interfaces\GoodsServiceInterfaces::class,
    'nodes' => [
        ['host' => env('GOODS_RPC_HOST','127.0.0.1'), 'port' => (int)env('GOODS_RPC_PORT')],
    ],
];
