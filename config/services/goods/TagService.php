<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

return [
    'name' => 'Goods::TagService',
    'service' => \App\Service\Goods\Interfaces\TagServiceInterfaces::class,
    'nodes' => [
        ['host' => env('GOODS_RPC_HOST','127.0.0.1'), 'port' => (int)env('GOODS_RPC_PORT')],
    ],
];
