<?php

return [
    'name' => 'Goods::GoodsCollectService',
    'service' => \App\Service\Goods\Interfaces\GoodsCollectServiceInterfaces::class,
    'nodes' => [
        ['host' => env('GOODS_RPC_HOST', '127.0.0.1'), 'port' => (int)env('GOODS_RPC_PORT')],
    ],
];
