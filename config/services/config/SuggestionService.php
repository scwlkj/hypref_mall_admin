<?php

return [
    'name' => 'Config::SuggestionService',
    'service' => \App\Service\Config\Interfaces\SuggestionServiceInterfaces::class,
    'nodes' => [
        ['host' => env('CONFIG_RPC_HOST', '127.0.0.1'), 'port' => (int)env('CONFIG_RPC_PORT')],
    ],
];
