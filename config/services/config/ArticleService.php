<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

return [
    'name' => 'Config::ArticleService',
    'service' => \App\Service\Config\Interfaces\ArticleServiceInterfaces::class,
    'nodes' => [
        ['host' => env('CONFIG_RPC_HOST','127.0.0.1'), 'port' => (int)env('CONFIG_RPC_PORT')],
    ],
];
