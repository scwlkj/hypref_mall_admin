<?php
/**
 * This file is part of Hyperf Mall.
 * 
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

declare(strict_types=1);

return [
    'name' => 'Config::AreaService',
    'service' => \App\Service\Config\Interfaces\AreaServiceInterfaces::class,
    'nodes' => [
        ['host' => env('CONFIG_RPC_HOST', '127.0.0.1'), 'port' => (int)env('CONFIG_RPC_PORT')],
    ],
];
