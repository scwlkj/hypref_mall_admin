<?php

return [
    'name' => 'Config::NotifyService',
    'service' => \App\Service\Config\Interfaces\NotifyServiceInterfaces::class,
    'nodes' => [
        ['host' => env('CONFIG_RPC_HOST', '127.0.0.1'), 'port' => (int)env('CONFIG_RPC_PORT')],
    ],
];
