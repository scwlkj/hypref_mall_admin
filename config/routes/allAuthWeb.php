<?php
declare(strict_types=1);
use Hyperf\HttpServer\Router\Router;

Router::addGroup("/app", function () {
    require __DIR__ . '/../routes/goods.php';
    require __DIR__ . '/../routes/article.php';
});
