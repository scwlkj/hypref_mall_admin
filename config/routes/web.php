<?php

\Hyperf\HttpServer\Router\Router::addGroup("", function () {
    require __DIR__ . '/../routes/authWeb.php';
}, ['middleware' => [\App\Middleware\AppAuthMiddleware::class]]);
\Hyperf\HttpServer\Router\Router::addGroup("", function () {
    require __DIR__ . '/../routes/noAuthWeb.php';
});

\Hyperf\HttpServer\Router\Router::addGroup("", function () {
    require __DIR__ . '/../routes/allAuthWeb.php';
}, ['middleware' => [\App\Middleware\GetMemberMiddleware::class]]);
