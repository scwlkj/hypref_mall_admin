<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */
use Hyperf\HttpServer\Router\Router;

\Hyperf\HttpServer\Router\Router::addGroup("/member", function () {
    Router::post('/openId', [\App\Controller\App\Member\MemberController::class, 'openId']);
});

