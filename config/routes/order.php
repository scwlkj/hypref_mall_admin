<?php
use Hyperf\HttpServer\Router\Router;

\Hyperf\HttpServer\Router\Router::addGroup("/order", function () {
    Router::post('/pre', [\App\Controller\App\Order\PreOrderController::class, 'initData']);
    Router::post('/initBuy', [\App\Controller\App\Order\PreOrderController::class, 'initBuy']);
    Router::post('/buy', [\App\Controller\App\Order\OrderController::class, 'buy']);
    Router::post('/create', [\App\Controller\App\Order\OrderController::class, 'create']);
    Router::post('/index', [\App\Controller\App\Order\OrderController::class, 'index']);
    Router::post('/cancel', [\App\Controller\App\Order\OrderController::class, 'cancel']);
    Router::get('/info', [\App\Controller\App\Order\OrderController::class, 'info']);
    Router::get('/detail', [\App\Controller\App\Order\OrderController::class, 'detail']);
    Router::post('/confirm', [\App\Controller\App\Order\OrderController::class, 'confirm']);
    Router::get('/goods', [\App\Controller\App\Order\OrderController::class, 'goods']);

    Router::post('/comment/add', [\App\Controller\App\Order\CommentController::class, 'add']);
});
