<?php
declare(strict_types=1);
/**
 * 验证登录信息接口
 */
use Hyperf\HttpServer\Router\Router;

Router::addGroup("/app", function () {
    Router::addGroup("/auth", function () {
        Router::post('/login', [App\Controller\App\Member\AuthController::class, 'login']);
    });

    require __DIR__ . '/../routes/memberNoAuth.php';
});


