<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */
use Hyperf\HttpServer\Router\Router;

\Hyperf\HttpServer\Router\Router::addGroup("/config", function () {
    Router::get('/article', [\App\Controller\App\Config\ArticleController::class, 'index']);
    Router::get('/articleDetail', [\App\Controller\App\Config\ArticleController::class, 'detail']);
});

