<?php
declare(strict_types=1);
/**
 * 验证登录信息接口
 */

\Hyperf\HttpServer\Router\Router::addGroup("/app", function () {
    require __DIR__ . '/../routes/cart.php';
    require __DIR__ . '/../routes/order.php';
    require __DIR__ . '/../routes/after.php';
    require __DIR__ . '/../routes/pay.php';
    require __DIR__ . '/../routes/member.php';
});


