<?php
use Hyperf\HttpServer\Router\Router;

\Hyperf\HttpServer\Router\Router::addGroup("/after", function () {
    Router::post('/apply', [\App\Controller\App\Order\AfterSaleController::class, 'apply']);
    Router::get('/list', [\App\Controller\App\Order\AfterSaleController::class, 'list']);
    Router::get('/detail', [\App\Controller\App\Order\AfterSaleController::class, 'detail']);
});
