<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */
use Hyperf\HttpServer\Router\Router;

\Hyperf\HttpServer\Router\Router::addGroup("/member", function () {
    require __DIR__ . '/../routes/address.php';

    Router::post('/detail', [\App\Controller\App\Member\MemberController::class, 'detail']);
    Router::post('/update', [\App\Controller\App\Member\MemberController::class, 'update']);
    Router::get('/info', [\App\Controller\App\Member\MemberController::class, 'info']);
    Router::get('/pointLog', [\App\Controller\App\Member\MemberController::class, 'pointLog']);
    Router::get('/message', [\App\Controller\App\Member\MemberController::class, 'message']);
    Router::post('/messageRead', [\App\Controller\App\Member\MemberController::class, 'messageRead']);
    Router::get('/collect', [\App\Controller\App\Member\MemberController::class, 'collect']);
    Router::get('/signData', [\App\Controller\App\Member\MemberController::class, 'signData']);
    Router::get('/signLog', [\App\Controller\App\Member\MemberController::class, 'signLog']);
    Router::post('/sign', [\App\Controller\App\Member\MemberController::class, 'sign']);
    Router::get('/coupon', [\App\Controller\App\Member\MemberController::class, 'coupon']);
});

