<?php

use Hyperf\HttpServer\Router\Router;

\Hyperf\HttpServer\Router\Router::addGroup("/address", function () {
    Router::post('/create', [\App\Controller\App\Member\AddressController::class, 'create']);
    Router::get('/index', [\App\Controller\App\Member\AddressController::class, 'index']);
    Router::post('/delete', [\App\Controller\App\Member\AddressController::class, 'delete']);
    Router::post('/update', [\App\Controller\App\Member\AddressController::class, 'update']);
    Router::get('/detail', [\App\Controller\App\Member\AddressController::class, 'detail']);
});
