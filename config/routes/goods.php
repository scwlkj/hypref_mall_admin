<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */
use Hyperf\HttpServer\Router\Router;

\Hyperf\HttpServer\Router\Router::addGroup("/goods", function () {
    Router::get('/list', [\App\Controller\App\Goods\IndexController::class, 'list']);
    Router::get('/detail', [\App\Controller\App\Goods\IndexController::class, 'detail']);
    Router::post('/collect', [\App\Controller\App\Goods\IndexController::class, 'collect']);
});
