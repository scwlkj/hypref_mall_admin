<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */
use Hyperf\HttpServer\Router\Router;

\Hyperf\HttpServer\Router\Router::addGroup("/cart", function () {
    Router::post('/add', [\App\Controller\App\Cart\CartController::class, 'add']);
    Router::get('/index', [\App\Controller\App\Cart\CartController::class, 'index']);
    Router::post('/incNum', [\App\Controller\App\Cart\CartController::class, 'incNum']);
    Router::post('/decNum', [\App\Controller\App\Cart\CartController::class, 'decNum']);
    Router::post('/editNumber', [\App\Controller\App\Cart\CartController::class, 'editNumber']);
    Router::post('/clear', [\App\Controller\App\Cart\CartController::class, 'clear']);
    Router::post('/removeByIds', [\App\Controller\App\Cart\CartController::class, 'removeByIds']);
});
