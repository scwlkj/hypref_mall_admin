<?php

namespace App\Service\Member;

use App\Service\BaseService;
use App\Service\Member\Interfaces\PointLogServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class PointLogService
 * @package  App\Service\Member
 * @method index(array $condition, array $field, int $page, int $size)
 */
class PointLogService extends BaseService
{
     /**
      * @Inject()
      * @var PointLogServiceInterfaces
      */
      protected $service;
}
