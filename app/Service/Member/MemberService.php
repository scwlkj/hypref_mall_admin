<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Member;


use App\Service\BaseService;
use App\Service\Member\Interfaces\MemberServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class MemberService
 * @package App\Service\Member
 * @method index(array $condition, array $field, int $page, int $size)
 * @method array registerPc(array $data)
 * @method array login(string $user_name, string $password)
 * @method int logout(string $token)
 */
class MemberService extends BaseService
{
    /**
     * @Inject()
     * @var MemberServiceInterfaces
     */
    protected $service;
}
