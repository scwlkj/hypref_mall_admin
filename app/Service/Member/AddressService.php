<?php

namespace App\Service\Member;

use App\Service\BaseService;
use App\Service\Member\Interfaces\AddressServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class AddressService
 * @package  App\Service\Member
 * @method index(array $condition, array $field, int $page, int $size)
 */
class AddressService extends BaseService
{
     /**
      * @Inject()
      * @var AddressServiceInterfaces
      */
      protected $service;
}
