<?php

namespace App\Service\Member\Interfaces;

interface SignServiceInterfaces
{
  public function initFunction(array $args): array;
}
