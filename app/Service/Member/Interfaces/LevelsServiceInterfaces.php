<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Member\Interfaces;


interface LevelsServiceInterfaces
{
    public function initFunction(array $args): array;
}
