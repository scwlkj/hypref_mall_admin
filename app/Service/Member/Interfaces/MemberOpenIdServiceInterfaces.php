<?php

namespace App\Service\Member\Interfaces;

interface MemberOpenIdServiceInterfaces
{
  public function initFunction(array $args): array;
}
