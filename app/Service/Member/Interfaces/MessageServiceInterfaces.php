<?php

namespace App\Service\Member\Interfaces;

interface MessageServiceInterfaces
{
  public function initFunction(array $args): array;
}
