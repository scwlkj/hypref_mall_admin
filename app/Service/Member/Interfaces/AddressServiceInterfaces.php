<?php

namespace App\Service\Member\Interfaces;

interface AddressServiceInterfaces
{
  public function initFunction(array $args): array;
}
