<?php

namespace App\Service\Member\Interfaces;

interface PointLogServiceInterfaces
{
  public function initFunction(array $args): array;
}
