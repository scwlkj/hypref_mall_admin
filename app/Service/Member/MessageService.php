<?php

namespace App\Service\Member;

use App\Service\BaseService;
use App\Service\Member\Interfaces\MessageServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class MessageService
 * @package  App\Service\Member
 * @method index(array $condition, array $field, int $page, int $size)
 */
class MessageService extends BaseService
{
     /**
      * @Inject()
      * @var MessageServiceInterfaces
      */
      protected $service;
}
