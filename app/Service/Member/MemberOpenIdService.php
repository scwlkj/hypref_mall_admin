<?php

namespace App\Service\Member;

use App\Service\BaseService;
use App\Service\Member\Interfaces\MemberOpenIdServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class MemberOpenIdService
 * @package  App\Service\Member
 * @method index(array $condition, array $field, int $page, int $size)
 */
class MemberOpenIdService extends BaseService
{
     /**
      * @Inject()
      * @var MemberOpenIdServiceInterfaces
      */
      protected $service;
}
