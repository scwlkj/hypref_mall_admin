<?php

namespace App\Service\Member;

use App\Service\BaseService;
use App\Service\Member\Interfaces\SignServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class SignService
 * @package  App\Service\Member
 * @method index(array $condition, array $field, int $page, int $size)
 * @method initData(int $member_id, array $param)
 * @method add(array $param)
 */
class SignService extends BaseService
{
     /**
      * @Inject()
      * @var SignServiceInterfaces
      */
      protected $service;
}
