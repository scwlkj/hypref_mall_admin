<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Member;


use App\Service\BaseService;
use App\Service\Member\Interfaces\LevelsServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class LevelsService
 * @package App\Service\Member
 * @method index(array $condition, array $field, int $page, int $size)

 */
class LevelsService extends BaseService
{
    /**
     * @Inject()
     * @var LevelsServiceInterfaces
     */
    protected $service;
}
