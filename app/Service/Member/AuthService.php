<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Member;


use App\Service\BaseService;
use App\Service\Member\Interfaces\AuthServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class MemberService
 * @package App\Service\Member
 * @method  login(int $type, array $data)        //登录
 * @method  getInfo(int $type, string $token)    //token换取信息
 */
class AuthService extends BaseService
{
    /**
     * @Inject()
     * @var AuthServiceInterfaces
     */
    protected $service;

    const TYPE_PLATFORM = 1;    //后台管理员
    const TYPE_APP = 2;         //前台用户
    const TYPE_PC = 3;         //PC端用户
}
