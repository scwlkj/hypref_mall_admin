<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

declare(strict_types=1);

namespace App\Service\Activity;

use App\Service\BaseService;
use App\Service\Activity\Interfaces\CouponServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class CouponService
 * @package  App\Service\Activity
 * @method index(array $condition, array $field, int $page, int $size)
 * @method update(int $id, array $data)
 * @method delete(int $id)
 * @method add(array $data)
 * @method getRows(array $condition, array $field)
 * @method getOne(array $condition, array $field = ['*'])
 * @method appIndex(int $page, int $size)
 */
class CouponService extends BaseService
{
     /**
      * @Inject()
      * @var CouponServiceInterfaces
      */
      protected $service;
}
