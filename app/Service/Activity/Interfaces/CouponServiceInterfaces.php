<?php
/**
 * This file is part of Hyperf Mall.
 * 
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

declare(strict_types=1);

namespace App\Service\Activity\Interfaces;

interface CouponServiceInterfaces
{
  public function initFunction(array $args): array;
}
