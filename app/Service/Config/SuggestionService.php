<?php

namespace App\Service\Config;

use App\Service\BaseService;
use App\Service\Config\Interfaces\SuggestionServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class SuggestionService
 * @package  App\Service\Config
 * @method index(array $condition, array $field, int $page, int $size)
 */
class SuggestionService extends BaseService
{
     /**
      * @Inject()
      * @var SuggestionServiceInterfaces
      */
      protected $service;
}
