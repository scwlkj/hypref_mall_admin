<?php

namespace App\Service\Config;

use App\Service\BaseService;
use App\Service\Config\Interfaces\ConfigServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class ConfigService
 * @package  App\Service\Config
 * @method index(array $condition, array $field, int $page, int $size)
 */
class ConfigService extends BaseService
{
     /**
      * @Inject()
      * @var ConfigServiceInterfaces
      */
      protected $service;
}
