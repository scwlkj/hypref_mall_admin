<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Config;


use App\Service\BaseService;
use App\Service\Config\Interfaces\ArticleServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class ArticleService
 * @package App\Service\Config
 * @method index(array $condition, array $field, int $page, int $size)
 */
class ArticleService extends BaseService
{
    /**
     * @Inject()
     * @var ArticleServiceInterfaces
     */
    protected $service;
}
