<?php

namespace App\Service\Config;

use App\Service\BaseService;
use App\Service\Config\Interfaces\HotSearchServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class HotSearchService
 * @package  App\Service\Config
 * @method index(array $condition, array $field, int $page, int $size)
 */
class HotSearchService extends BaseService
{
     /**
      * @Inject()
      * @var HotSearchServiceInterfaces
      */
      protected $service;
}
