<?php

namespace App\Service\Config;

use App\Service\BaseService;
use App\Service\Config\Interfaces\IndexMenuServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class IndexMenuService
 * @package  App\Service\Config
 * @method index(array $condition, array $field, int $page, int $size)
 */
class IndexMenuService extends BaseService
{
     /**
      * @Inject()
      * @var IndexMenuServiceInterfaces
      */
      protected $service;
}
