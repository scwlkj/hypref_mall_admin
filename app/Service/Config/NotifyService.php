<?php

namespace App\Service\Config;

use App\Service\BaseService;
use App\Service\Config\Interfaces\NotifyServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class NotifyService
 * @package  App\Service\Config
 * @method index(array $condition, array $field, int $page, int $size)
 * @method array appIndexList()
 */
class NotifyService extends BaseService
{
     /**
      * @Inject()
      * @var NotifyServiceInterfaces
      */
      protected $service;
}
