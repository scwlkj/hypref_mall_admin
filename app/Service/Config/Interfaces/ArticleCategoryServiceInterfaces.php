<?php

namespace App\Service\Config\Interfaces;

interface ArticleCategoryServiceInterfaces
{
  public function initFunction(array $args): array;
}
