<?php

namespace App\Service\Config\Interfaces;

interface HotSearchServiceInterfaces
{
  public function initFunction(array $args): array;
}
