<?php

namespace App\Service\Config\Interfaces;

interface ConfigServiceInterfaces
{
  public function initFunction(array $args): array;
}
