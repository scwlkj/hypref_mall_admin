<?php

namespace App\Service\Config\Interfaces;

interface IndexMenuServiceInterfaces
{
  public function initFunction(array $args): array;
}
