<?php

namespace App\Service\Config\Interfaces;

interface AdvServiceInterfaces
{
  public function initFunction(array $args): array;
}
