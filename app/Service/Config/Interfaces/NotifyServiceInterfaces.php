<?php

namespace App\Service\Config\Interfaces;

interface NotifyServiceInterfaces
{
    public function initFunction(array $args): array;
}
