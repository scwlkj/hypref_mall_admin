<?php

namespace App\Service\Config\Interfaces;

interface SuggestionServiceInterfaces
{
  public function initFunction(array $args): array;
}
