<?php

namespace App\Service\Config\Interfaces;

interface ExpressServiceInterfaces
{
  public function initFunction(array $args): array;
}
