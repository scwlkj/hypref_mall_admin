<?php

namespace App\Service\Config;

use App\Service\BaseService;
use App\Service\Config\Interfaces\AdvServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class AdvService
 * @package  App\Service\Config
 * @method index(array $condition, array $field, int $page, int $size)
 * @method array indexAdv()
 */
class AdvService extends BaseService
{
     /**
      * @Inject()
      * @var AdvServiceInterfaces
      */
      protected $service;
}
