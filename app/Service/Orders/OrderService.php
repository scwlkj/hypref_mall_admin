<?php

namespace App\Service\Orders;

use App\Service\BaseService;
use App\Service\Orders\Interfaces\OrderServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class OrderService
 * @package  App\Service\Orders
 * @method index(array $condition, array $field, int $page, int $size)
 */
class OrderService extends BaseService
{
     /**
      * @Inject()
      * @var OrderServiceInterfaces
      */
      protected $service;
}
