<?php

namespace App\Service\Orders;

use App\Service\BaseService;
use App\Service\Orders\Interfaces\AfterSaleServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class AfterSaleService
 * @package  App\Service\Orders
 * @method index(array $condition, array $field, int $page, int $size)
 */
class AfterSaleService extends BaseService
{
     /**
      * @Inject()
      * @var AfterSaleServiceInterfaces
      */
      protected $service;
}
