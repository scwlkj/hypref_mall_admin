<?php

namespace App\Service\Orders;

use App\Service\BaseService;
use App\Service\Orders\Interfaces\PreOrderServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class PreOrderService
 * @package  App\Service\Orders
 * @method index(array $condition, array $field, int $page, int $size)
 */
class PreOrderService extends BaseService
{
     /**
      * @Inject()
      * @var PreOrderServiceInterfaces
      */
      protected $service;
}
