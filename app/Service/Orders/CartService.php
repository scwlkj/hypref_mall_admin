<?php

namespace App\Service\Orders;

use App\Service\BaseService;
use App\Service\Orders\Interfaces\CartServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class CartService
 * @package  App\Service\Orders
 * @method getList(int $member_id)
 * @method clear(int $member_id)
 * @method removeByIds(array $ids)
 * @method editNumber($id, $num)
 * @method decNum($id, $num)
 * @method incNum(int $id, int $num)
 */
class CartService extends BaseService
{
     /**
      * @Inject()
      * @var CartServiceInterfaces
      */
      protected $service;
}
