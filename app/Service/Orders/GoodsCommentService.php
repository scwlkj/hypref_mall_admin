<?php

namespace App\Service\Orders;

use App\Service\BaseService;
use App\Service\Orders\Interfaces\GoodsCommentServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class GoodsCommentService
 * @package  App\Service\Orders
 * @method index(array $condition, array $field, int $page, int $size)
 */
class GoodsCommentService extends BaseService
{
     /**
      * @Inject()
      * @var GoodsCommentServiceInterfaces
      */
      protected $service;
}
