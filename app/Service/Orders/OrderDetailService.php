<?php

namespace App\Service\Orders;

use App\Service\BaseService;
use App\Service\Orders\Interfaces\OrderDetailServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class OrderDetailService
 * @package  App\Service\Orders
 * @method index(array $condition, array $field, int $page, int $size)
 */
class OrderDetailService extends BaseService
{
     /**
      * @Inject()
      * @var OrderDetailServiceInterfaces
      */
      protected $service;
}
