<?php

namespace App\Service\Orders;

use App\Service\BaseService;
use App\Service\Orders\Interfaces\CommentServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class CommentService
 * @package  App\Service\Orders
 * @method index(array $condition, array $field, int $page, int $size)
 */
class CommentService extends BaseService
{
     /**
      * @Inject()
      * @var CommentServiceInterfaces
      */
      protected $service;
}
