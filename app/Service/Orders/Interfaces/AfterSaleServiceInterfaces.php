<?php

namespace App\Service\Orders\Interfaces;

interface AfterSaleServiceInterfaces
{
  public function initFunction(array $args): array;
}
