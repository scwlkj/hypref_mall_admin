<?php

namespace App\Service\Orders\Interfaces;

interface CartServiceInterfaces
{
  public function initFunction(array $args): array;
}
