<?php

namespace App\Service\Orders\Interfaces;

interface OrderDetailServiceInterfaces
{
  public function initFunction(array $args): array;
}
