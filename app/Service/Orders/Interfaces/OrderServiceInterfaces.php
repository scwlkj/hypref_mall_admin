<?php

namespace App\Service\Orders\Interfaces;

interface OrderServiceInterfaces
{
  public function initFunction(array $args): array;
}
