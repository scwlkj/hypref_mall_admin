<?php

namespace App\Service\Orders\Interfaces;

interface PreOrderServiceInterfaces
{
  public function initFunction(array $args): array;
}
