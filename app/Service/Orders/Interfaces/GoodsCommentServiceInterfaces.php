<?php

namespace App\Service\Orders\Interfaces;

interface GoodsCommentServiceInterfaces
{
  public function initFunction(array $args): array;
}
