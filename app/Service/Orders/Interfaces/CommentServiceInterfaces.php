<?php

namespace App\Service\Orders\Interfaces;

interface CommentServiceInterfaces
{
  public function initFunction(array $args): array;
}
