<?php

namespace App\Service\Goods\Interfaces;

interface GoodsCollectServiceInterfaces
{
  public function initFunction(array $args): array;
}
