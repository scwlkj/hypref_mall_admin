<?php

namespace App\Service\Goods;

use App\Service\BaseService;
use App\Service\Goods\Interfaces\GoodsCollectServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class GoodsCollectService
 * @package  App\Service\Goods
 * @method index(array $condition, array $field, int $page, int $size)
 * @method array collect(int $goods_id, int $member_id)
 * @method array memberCollect(int $member_id, $page, $size)
 */
class GoodsCollectService extends BaseService
{
     /**
      * @Inject()
      * @var GoodsCollectServiceInterfaces
      */
      protected $service;
}
