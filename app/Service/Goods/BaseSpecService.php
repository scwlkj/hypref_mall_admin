<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Goods;


use App\Service\BaseService;
use App\Service\Goods\Interfaces\BaseSpecServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class BaseSpecService
 * @package App\Service\Goods
 * @method index(array $condition, array $field, int $page, int $size)
 * @method update(int $id, array $param)
 * @method delete(int $id)
 * @method detail(int $id)
 */
class BaseSpecService extends BaseService
{
    /**
     * @Inject()
     * @var BaseSpecServiceInterfaces
     */
    protected $service;
}
