<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Goods;


use App\Service\BaseService;
use App\Service\Goods\Interfaces\CateServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class CateService
 * @package App\Service\Goods
 * @method index(array $condition, array $field, int $page, int $size)
 * @method selectTree()
 * @method tree()
 * @method update(int $id, array $param)
 * @method delete(int $id)
 */
class CateService extends BaseService
{
    /**
     * @Inject()
     * @var CateServiceInterfaces
     */
    protected $service;
}
