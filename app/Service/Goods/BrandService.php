<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Goods;


use App\Service\BaseService;
use App\Service\Goods\Interfaces\BrandServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class BrandService
 * @package App\Service\Goods
 * @method index(array $condition, array $field, int $page, int $size)
 * @method update(int $id, array $param)
 * @method delete(int $id)
 */
class BrandService extends BaseService
{
    /**
     * @Inject()
     * @var BrandServiceInterfaces
     */
    protected $service;
}
