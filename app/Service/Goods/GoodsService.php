<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Service\Goods;


use App\Service\BaseService;
use App\Service\Goods\Interfaces\GoodsServiceInterfaces;
use Hyperf\Di\Annotation\Inject;

/**
 * Class GoodsService
 * @package App\Service\Goods
 * @method index(array $condition, array $field, int $page, int $size)
 * @method array initData()
 * @method array appIndexList()
 * @method array appList(array $param)
 * @method array appDetail(int $id, int $member_id)
 */
class GoodsService extends BaseService
{
    /**
     * @Inject()
     * @var GoodsServiceInterfaces
     */
    protected $service;
}
