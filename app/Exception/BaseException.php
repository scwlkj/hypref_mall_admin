<?php
namespace App\Exception;

use Exception;

class BaseException extends Exception
{
    const SYSTEM_ERROR = 10000;
    const BASE_ERROR = 10400;
    const TOKEN_ERROR = 4001;    //登录失效

    const ERROR_MSG = [
        self::SYSTEM_ERROR => '系统繁忙,请稍后重试',
        self::BASE_ERROR=> '%s',
        self::TOKEN_ERROR => '无效令牌，需要重新获取',
    ];

    /**
     * @param int $code
     * @param string $errorMsg
     */
    public function __construct($code = 0, $errorMsg = '')
    {
        $message = $this->getReason($code, $errorMsg);
        parent::__construct($message, $code, null);
    }

    /**
     * 获取错误代码描述
     * @param $code
     * @param string $errorMsg
     * @return string
     */
    public function getReason($code, $errorMsg = '')
    {
        $message = self::ERROR_MSG[$code] ?? 'Unknown error code';
        return sprintf($message, $errorMsg);
    }
}
