<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;
use App\Service\Member\AuthService;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * Note: 模拟数据-admin
 * @AutoController()
 * Class IndexController
 * @package App\Controller
 */
class DemoController extends AbstractController
{
    public function login()
    {
        $token = AuthService::getInstance()->login(2,['mobile' => '13131316313', 'code' => '234567']);
        $data = [
            'code'=>200,
            'data'=>[
                'admin'=>[
                    'token'=>$token
                ],
                'editor'=>[
                    'token'=>'editor-token'
                ]
            ]
        ];

        return json_encode($data);
    }

    public function info()
    {
        $data = [
            'code'=>200,
            'data'=>[
                'roles'=>['admin'],
                'introduction'=>'XXXXXXXXXXX',
                'avatar'=>'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
                'name'=>'UUUUUUUU',
            ]
        ];

        return json_encode($data);
    }
}
