<?php

namespace App\Controller\App\Member;

use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Service\Member\AuthService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class AuthController extends BaseController
{

    /**
     * @Inject
     * @var AuthService
     */
    protected $authService;

    public function login(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'mobile' => 'required|regex:/^(1[3-9])\d{9}$/',
                'code' => 'integer|size:6',
                'password' => 'string|min:6',
            ],
            [
                'mobile.*' => '手机号必须填写',
                'code.*' => '验证码输入有误',
                'password.*' => '密码输入有误',
            ]
        );

        $param = array_only($param, ['mobile', 'code', 'password']);

        return success($this->authService->login(AuthService::TYPE_APP, $param));
    }


}
