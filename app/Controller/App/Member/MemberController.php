<?php

namespace App\Controller\App\Member;

use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Activity\CouponReceiveService;
use App\Service\Goods\GoodsCollectService;
use App\Service\Member\AddressService;
use App\Service\Member\MemberOpenIdService;
use App\Service\Member\MemberService;
use App\Service\Member\MessageService;
use App\Service\Member\PointLogService;
use App\Service\Member\SignService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 */
class MemberController extends BaseController
{

    /**
     * @Inject
     * @var MemberService
     */
    protected MemberService $memberService;

    /**
     * @Inject
     * @var GoodsCollectService
     */
    protected GoodsCollectService $goodsCollectService;

    /**
     * @Inject
     * @var SignService
     */
    protected SignService $signService;

    /**
     * @Inject
     * @var PointLogService
     */
    protected PointLogService $pointLogService;

    /**
     * @Inject
     * @var MessageService
     */
    protected MessageService $messageService;

    /**
     * @Inject
     * @var MemberOpenIdService
     */
    protected MemberOpenIdService $memberOpenIdService;


    /**
     * @Inject
     * @var CouponReceiveService
     */
    protected CouponReceiveService $couponReceiveService;

    public function detail():array
    {
        return success(get_user_info());
    }

    public function update():array
    {
        $params = array_only($this->request->all(),[
            'username','image','sex','birth'
        ]);
        $params = $this->inspectHelper->check(
            $params,
            [
                'username' => 'string|min:1',
                'image' => 'string|min:1',
                'sex' => 'integer|between:0,2',
                'birth' => 'date',
            ],
            [
                'username.*' => '昵称输入有误',
                'image.*' => '图片信息有误',
                'sex.*' => '未知性别',
                'birth.*' => '生日格式错误'
            ]
        );
        if (isset( $params['birth'])) {
            $params['birth'] = strtotime($params['birth']);
        }
        return success($this->memberService->update( get_user_info()['id'] ?? 0,$params));
    }

    /**
     * Notes: 我的收藏
     * Author: Bruce.z
     * DateTime: 2022/7/18 18:14
     * @return array
     * @throws BaseException
     */
    public function collect():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
            ],
            [
                'page.*' => '参数传递错误',
                'size.*' => '参数传递错误',
            ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->goodsCollectService->memberCollect(get_user_info()['id'], $param['page'], $param['size']));
    }

    /**
     * Notes: 签到
     * Author: Bruce.z
     * DateTime: 2022/7/18 18:19
     * @return array
     * @throws BaseException
     */
    public function sign(): array
    {
        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        return success($this->signService->add(['member_id'=>get_user_info()['id']]));
    }

    /**
     * Notes: 获取用户指定月份 签到记录
     * Author: Bruce.z
     * DateTime: 2022/7/18 18:16
     * @return array
     * @throws BaseException
     */
    public function signLog(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['date' => 'required|string'],
            ['date.*' => 'date参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->signService->initData(get_user_info()['id'], $param['date']));
    }

    /**
     * Notes: 用户签到数据
     * Author: Bruce.z
     * DateTime: 2022/7/19 9:14
     * @return array
     * @throws BaseException
     */
    public function signData(): array
    {
        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        $re = $this->signService->getOne(['member_id'=>get_user_info()['id']],['days']);

        return success(['sign_days'=>$re['days']??0,'point'=>1]);
    }

    /**
     * Notes: 会员中心首页
     * Author: Bruce.z
     * DateTime: 2022/7/19 14:08
     * @return array
     * @throws BaseException
     */
    public function info(): array
    {
        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->memberService->info(get_user_info()['id']));
    }

    /**
     * Notes: 积分记录
     * Author: Bruce.z
     * DateTime: 2022/7/19 16:42
     * @return array
     * @throws BaseException
     */
    public function pointLog(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['page' => 'required|integer|min:1'],
            ['page.*' => 'page参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->pointLogService->index(['member_id'=>get_user_info()['id']],['type','point','remark','create_time'], $param['page'], 10));
    }

    /**
     * Notes: 站内信消息
     * Author: Bruce.z
     * DateTime: 2022/7/20 11:14
     * @return array
     * @throws BaseException
     */
    public function message():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['page' => 'required|integer|min:1'],
            ['page.*' => 'page参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->messageService->index(['member_id'=>get_user_info()['id']],['title','status','oid','content','create_time','sender_type'], $param['page'], 10));
    }

    /**
     * Notes: 消息已读
     * Author: Bruce.z
     * DateTime: 2022/7/20 13:31
     * @return array
     * @throws BaseException
     */
    public function messageRead(): array
    {
        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        return success($this->messageService->messageRead(get_user_info()['id']));
    }

    /**
     * Notes: code 换区 openid
     * Author: Bruce.z
     * DateTime: 2022/7/29 18:33
     * @return array
     * @throws BaseException
     */
    public function openId(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'code' => 'required|string',
                'avatarUrl' => 'required|string',
                'nickName' => 'required|string'
            ],
            ['*.*' => '参数错误' ]
        );

        return success($this->memberOpenIdService->getOpenId($param['code'], $param['avatarUrl'], $param['nickName']));
    }

    /**
     * Notes:会员领取的优惠券列表
     * Author: Bruce.z
     * DateTime: 2022/9/13 10:56
     * @return array
     * @throws BaseException
     */
    public function coupon(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
                'status' => 'required|integer|min:0',
            ],
            [
                'page.*' => 'page参数错误',
                'size.*' => 'size参数错误',
                'status.*' => 'status参数错误'
            ]
        );

        $_condition = [
            'member_id'=>get_user_info()['id'],
            'status'=>$param['status']
        ];

        $data = $this->couponReceiveService->memberCoupon($_condition,$param['page'], $param['size']);
        return success($data);
    }
}
