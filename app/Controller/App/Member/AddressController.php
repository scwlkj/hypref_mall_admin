<?php

namespace App\Controller\App\Member;

use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Service\Member\AddressService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 */
class AddressController extends BaseController
{
    /**
     * @Inject
     * @var AddressService
     */
    protected $addressService;

    public function index():array
    {
        // TODO 修改返回参数
        return success($this->addressService->getRows(['member_id'=>get_user_info()['id'] ?? 0], ['*']));
    }

    public function create()
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'ship_province_id' => 'integer|min:1|required',
                'ship_county_id' => 'integer|min:1|required',
                'ship_name' => 'string|required',
                'ship_mobile' => 'string|required',
                'lng' => 'required',
            ],
            [
                'ship_province_id.*' => '省市区不能为空',
                'ship_county_id.*' => '省市区不能为空',
                'ship_name' => '收货人名称错误',
                'ship_mobile.*' => '收货人名称错误',
                'lng.*' => '定位信息不能为空',
            ]
        );

        $param['member_id'] = get_user_info()['id'] ?? 0;
        return success($this->addressService->add($param));
    }

    public function delete()
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'integer|min:1'],
            ['id.*' => '参数传递错误']
        );

        return success($this->addressService->delete($param['id']));
    }

    public function update()
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'integer|min:1',
                'ship_province_id' => 'integer|min:1|required',
                'ship_county_id' => 'integer|min:1|required',
                'ship_name' => 'string|required',
                'ship_mobile' => 'string|required',
                'lng' => 'required',
            ],
            [
                'id.*' => '参数传递错误',
                'ship_province_id.*' => '省市区不能为空',
                'ship_county_id.*' => '省市区不能为空',
                'ship_name' => '收货人名称错误',
                'ship_mobile.*' => '收货人名称错误',
                'lng.*' => '定位信息不能为空',
            ]
        );

        return success($this->addressService->update($param['id'], $param));
    }

    public function detail() :array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'integer|min:1'],
            ['id.*' => '参数传递错误']
        );
        return success($this->addressService->getOne(['id'=>$param['id']]));
    }
}
