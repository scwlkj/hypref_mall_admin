<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\App;


use App\Common\InspectHelper;
use App\Controller\AbstractController;
use App\Utils\Log;
use Hyperf\Utils\Context;
use Hyperf\Di\Annotation\Inject;

/**
 * Class BaseController
 * @package App\Controller\App
 */
class BaseController extends AbstractController
{
    /**
     * @Inject
     * @var InspectHelper
     */
    protected InspectHelper $inspectHelper;
}
