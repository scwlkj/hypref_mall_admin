<?php

namespace App\Controller\App\Order;


use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\AfterSaleService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 */
class AfterSaleController extends BaseController
{
    /**
     * @Inject
     * @var AfterSaleService
     */
    protected AfterSaleService $afterSaleService;

    /**
     * Notes: C端 - 申请订单售后
     * Author: Bruce.z
     * DateTime: 2022/8/18 15:50
     * @return array
     * @throws BaseException
     */
    public function apply(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'oid' => 'required',
                'order_detail_id' => 'required',
                'return_money' => 'required',
                'goods_id' => 'required',
                'product_id' => 'required',
                'reason' => 'required',
                'description' => 'required',
                'image' => 'required',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $param['member_id'] = get_user_info()['id'];
        $data = $this->afterSaleService->add($param);
        return success($data);
    }

    /**
     * Notes: 我的售后列表
     * Author: Bruce.z
     * DateTime: 2022/8/19 9:49
     * @return array
     * @throws BaseException
     */
    public function list(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required',
                'size' => 'required',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $param['member_id'] = get_user_info()['id'];
        $data = $this->afterSaleService->appList($param);
        return success($data);
    }

    /**
     * Notes: 订单详情
     * Author: Bruce.z
     * DateTime: 2022/8/19 10:24
     * @return array
     * @throws BaseException
     */
    public function detail(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'number' => 'required',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $data = $this->afterSaleService->appDetail($param['number']);
        return success($data);
    }

    /**
     * Notes: 根据订单详情id 获取申请退款页面详情
     * Author: Bruce.z
     * DateTime: 2022/10/26 17:22
     * @param int $id
     * @return array
     * @throws BaseException
     */
    public function applyPre(int $id): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $data = $this->afterSaleService->applyPre($param['id']);
        return success($data);
    }
}
