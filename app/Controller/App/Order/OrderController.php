<?php

namespace App\Controller\App\Order;


use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\OrderDetailService;
use App\Service\Orders\OrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 */
class OrderController extends BaseController
{
    /**
     * @Inject()
     * @var OrderService
     */
    protected OrderService $orderService;

    /**
     * @Inject
     * @var OrderDetailService
     */
    protected OrderDetailService $orderDetailService;

    /**
     * Notes: 创建订单
     * Author: Bruce.z
     * DateTime: 2022/7/14 9:01
     * @return array
     * @throws BaseException
     */
    public function create(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'cartId' => 'array|required',
                'cartId.*' => 'integer|required',
                'address_id' => 'integer|required',
                'coupon_receive_id' => 'integer',
            ],
            ['cartId.*' => '参数传递错误','address_id.*' => '参数传递错误']
        );

        $param['member_id'] = get_user_info()['id'] ?? 0;
        return success( $this->orderService->add($param));
    }

    /**
     * Notes: 直接购买
     * Author: Bruce.z
     * DateTime: 2022/7/22 10:04
     * @return array
     * @throws BaseException
     */
    public function buy(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'product_id' => 'integer|required|min:1',
                'number' => 'integer|required|min:1',
                'address_id' => 'integer|required|min:1',
                'coupon_receive_id' => 'integer',
            ],
            [
                'product_id.*' => '参数传递错误',
                'number.*' => '参数传递错误',
                'address_id.*' => '参数传递错误'
            ]
        );

        $param['member_id'] = get_user_info()['id'] ?? 0;
        return success( $this->orderService->buy($param));
    }

    /**
     * Notes: 会员订单列表页
     * Author: Bruce.z
     * DateTime: 2022/7/14 9:01
     * @return array
     * @throws BaseException
     */
    public function index():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'integer|required|min:1',
                'size' => 'integer|required|min:1',
                'status' => 'integer',
            ],
            ['page.*' => 'page参数错误','size.*' => 'size参数错误','status.*' => 'status参数错误']
        );

        isset($param['status']) && !is_null($param['status']) && $condition['status'] = $param['status'];
        $condition['member_id'] = get_user_info()['id'] ?? 0;
        return success( $this->orderService->appIndex($condition,$param['page'], $param['size']));
    }

    /**
     * Notes: 支付订单详情
     * Author: Bruce.z
     * DateTime: 2022/7/14 11:05
     * @return array
     * @throws BaseException
     */
    public function info():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['oid' => 'required'],
            ['oid.*' => 'oid参数错误']
        );

        return success( $this->orderService->info($param['oid']));
    }

    /**
     * Notes: c端 取消订单
     * Author: Bruce.z
     * DateTime: 2022/7/15 14:27
     * @return array
     * @throws BaseException
     */
    public function cancel(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['oid' => 'required'],
            ['oid.*' => 'oid参数错误']
        );

        return success($this->orderService->cancel($param['oid']));
    }

    /**
     * Notes: c端 获取详情
     * Author: Bruce.z
     * DateTime: 2022/7/15 15:52
     * @return array
     * @throws BaseException
     */
    public function detail(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['oid' => 'required'],
            ['oid.*' => 'oid参数错误']
        );

        return success($this->orderService->appDetail($param['oid']));
    }

    /**
     * Notes: c端确认收货
     * Author: Bruce.z
     * DateTime: 2022/8/16 18:14
     * @return array
     * @throws BaseException
     */
    public function confirm(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['oid' => 'required'],
            ['oid.*' => 'oid参数错误' ]
        );

        return success($this->orderService->confirmGoods($param['oid']));
    }

    /**
     * Notes: C端 - 订单商品
     * Author: Bruce.z
     * DateTime: 2022/8/17 14:23
     * @return array
     * @throws BaseException
     */
    public function goods(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['oid' => 'required'],
            ['oid.*' => 'oid参数错误' ]
        );

        $data = $this->orderDetailService->getRows(['oid'=>$param['oid']], ['id','goods_name','num','spec','retail_price','total_price','price','product_image','delivery_status','goods_id','product_id']);

        return success($data);
    }

}
