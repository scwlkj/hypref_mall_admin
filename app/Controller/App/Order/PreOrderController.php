<?php

namespace App\Controller\App\Order;

use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\PreOrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 */
class PreOrderController extends BaseController
{
    /**
     * @Inject
     * @var PreOrderService
     */
    protected $preOrderService;

    /**
     * Notes: 预提交订单
     * Author: Bruce.z
     * DateTime: 2022/7/9 17:07
     * @return array
     * @throws BaseException
     */
    public function initData(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['ids' => 'array|required','ids.*' => 'integer|required'],
            ['ids.*' => '参数传递错误']
        );

        $data = $this->preOrderService->initData($param['ids'],  get_user_info()['id'] ?? 0);
        return success($data);
    }

    /**
     * Notes: 立即购买
     * Author: Bruce.z
     * @return array
     * @throws BaseException
     */
    public function initBuy(): array
    {
        $param = array_only($this->request->all(), ['goods_id','product_id','goods_number']);
        $param = $this->inspectHelper->check(
            $param,
            [
                'goods_id' => 'integer|required|min:1',
                'product_id' => 'integer|required|min:1',
                'goods_number' => 'integer|required|min:1'
            ],
            [
                'goods_id.*' => 'goods_id参数传递错误',
                'product_id.*' => 'goods_id参数传递错误',
                'goods_number.*' => 'goods_id参数传递错误',
            ]
        );

        $data = $this->preOrderService->initBuy($param, get_user_info()['id'] ?? 0);
        return success($data);
    }
}
