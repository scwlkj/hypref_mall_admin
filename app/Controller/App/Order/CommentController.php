<?php

namespace App\Controller\App\Order;


use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\CommentService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 */
class CommentController extends BaseController
{

    /**
     * @Inject
     * @var CommentService
     */
    protected CommentService $commentService;

    /**
     * Notes: 添加评论
     * Author: Bruce.z
     * DateTime: 2022/8/17 20:22
     * @return array
     * @throws BaseException
     */
    public function add(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'oid' => 'required',
                'formData'=>'required|array',
                'formData.*'=>[
                    'oid' => 'required',
                ],
                'logistics'=>'required',
                'service'=>'required',
            ],
            ['*.required' => ':attribute 为必填字段' ]
        );

        $param['member_id'] = get_user_info()['id'];
        $re = $this->commentService->add($param);
        return success($re);
    }

}
