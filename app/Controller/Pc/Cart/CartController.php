<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Pc\Cart;


use App\Common\InspectHelper;
use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\CartService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcAuthMiddleware;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(PcAuthMiddleware::class)
 * })
 */
class CartController extends BaseController
{

    /**
     * @Inject
     * @var CartService
     */
    protected $cartService;

    /**
     * Notes: 加入购物车
     * @RequestMapping(path="add", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/6/27 17:46
     * @return array
     * @throws BaseException
     */
    public function add(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'goods_id' => 'required|integer|min:1',
                'number' => 'required|integer|min:1',
                'product_id' => 'required|integer|min:0',
            ],
            [
                'goods_id.*' => 'goods_id参数传递错误',
                'number.*' => 'number参数传递错误',
                'product_id.*' => 'product_id参数传递错误'
            ]
        );

        $data = [
            'member_id'=>get_user_info()['id'] ?? 0,
            'goods_id'=> $param['goods_id'],
            'product_id'=> $param['product_id'],
            'number'=> $param['number'],
        ];

        return success($this->cartService->add($data));
    }

    /**
     * Notes: 购物车列表
     * @RequestMapping(path="list", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/7/1 10:36
     * @return array
     */
    public function list(): array
    {
        return success($this->cartService->getList(get_user_info()['id'] ?? 0));
    }

    /**
     * Notes: 清空购物车
     * @RequestMapping(path="clear", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/7/1 10:36
     * @return array
     */
    public function clear(): array
    {
        return success($this->cartService->clear(get_user_info()['id'] ?? 0));
    }

    /**
     * Notes: 编辑单个购物车商品数量
     * @RequestMapping(path="editNumber", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/7/1 10:37
     * @return array
     * @throws BaseException
     */
    public function editNumber(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
                'number' => 'required|integer|min:1',
            ],
            [
                'id.*' => 'id参数传递错误',
                'number.*' => 'number参数传递错误'
            ]
        );
        return success($this->cartService->editNumber($param['id'], $param['number']));
    }

    /**
     * Notes: 递减单个数量
     * Author: Bruce.z
     * DateTime: 2022/7/1 10:37
     * @return array
     * @throws BaseException
     */
    public function decNum(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
                'number' => 'required|integer|min:1',
            ],
            [
                'id.*' => 'id参数传递错误',
                'number.*' => 'number参数传递错误'
            ]
        );
        return success($this->cartService->decNum($param['id'], $param['number']));
    }

    /**
     * Notes: 递增单个数量
     * Author: Bruce.z
     * DateTime: 2022/7/1 10:37
     * @return array
     * @throws BaseException
     */
    public function incNum(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
                'number' => 'required|integer|min:1',
            ],
            [
                'id.*' => 'id参数传递错误',
                'number.*' => 'number参数传递错误'
            ]
        );
        return success($this->cartService->incNum($param['id'], $param['number']));
    }

    /**
     * Notes: 批量删除购物车商品
     * @RequestMapping(path="deletCarts", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/17 17:01
     * @return array
     * @throws BaseException
     */
    public function removeByIds(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'ids' => 'required|array',
            ],
            [
                'ids.*' => 'ids参数传递错误',
            ]
        );

        return success($this->cartService->removeByIds($param['ids']));
    }

    /**
     * Notes: 设置选中状态
     * @RequestMapping(path="setChecked", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/17 17:47
     * @return array
     * @throws BaseException
     */
    public function setChecked(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'type' => 'required|int',
                'checked' => 'required|bool',
                'id' => 'int',
            ],
            [
                'type.*' => 'type参数传递错误',
                'checked.*' => 'checked参数传递错误',
            ]
        );

        $member_id = get_user_info()['id'] ?? 0;
        return success($this->cartService->setChecked($member_id, $param['type'], $param['checked'], $param['id']??0));
    }
}
