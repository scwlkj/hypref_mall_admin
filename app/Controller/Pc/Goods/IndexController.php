<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Pc\Goods;


use App\Common\InspectHelper;
use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Goods\GoodsCollectService;
use App\Service\Goods\GoodsService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcGetMemberMiddleware;

/**
 * Class IndexController
 * @Middlewares({
 *     @Middleware(PcGetMemberMiddleware::class)
 * })
 * @package App\Controller\Pc\Goods
 * @Controller()
 */
class IndexController extends BaseController
{

    /**
     * @Inject
     * @var GoodsService
     */
    protected GoodsService $goodsService;

    /**
     * @Inject
     * @var GoodsCollectService
     */
    protected GoodsCollectService $goodsCollectService;

    /**
     * Notes: 列表页
     * @RequestMapping(path="list", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/6/14 15:26
     */
    public function list(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'category_id' => 'integer',
                'top_id' => 'integer',
                'keyword' => 'string',
                'sort' => 'integer',
                'page' => 'integer|required|min:1',
                'size' => 'integer|required|min:1',
            ],
            ['*.*' => '参数传递错误']
        );
        $param['page'] = intval($param['page']) ?? 1;
        $param['size'] = intval($param['size']) ?? 12;

        return success($this->goodsService->appList($param));
    }

    /**
     * Notes: 详情页
     * @RequestMapping(path="detail", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/6/14 15:26
     */
    public function detail(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'required|integer|min:1'],
            ['id.*' => '参数传递错误']
        );

        return success($this->goodsService->appDetail($param['id'], get_user_info()['id'] ?? 0));
    }

    /**
     * Notes: 添加/移除 收藏
     *  @RequestMapping(path="collect", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/7/18 9:49
     * @return array
     * @throws BaseException
     */
    public function collect(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['goods_id' => 'required|integer|min:1'],
            ['goods_id.*' => '参数传递错误']
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->goodsCollectService->collect($param['goods_id'], get_user_info()['id']));
    }
}
