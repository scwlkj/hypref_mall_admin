<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Pc\Goods;


use App\Common\InspectHelper;
use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Goods\GoodsService;
use App\Service\Orders\GoodsCommentService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 */
class CommentController extends BaseController
{

    /**
     * @Inject
     * @var GoodsService
     */
    protected GoodsService $goodsService;

    /**
     * @Inject
     * @var GoodsCommentService
     */
    protected GoodsCommentService $goodsCommentService;

    /**
     * Notes: 商品评论列表
     * @RequestMapping(path="", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/9/27 11:19
     * @return array
     * @throws BaseException
     */
    public function list(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
                'goods_mark' => 'integer',
                'goods_id' => 'required|integer|min:1',
            ],
            [
                'page.*' => 'page参数传递错误',
                'size.*' => 'size参数传递错误',
            ]
        );
        $page = $param['page'];unset($param['page']);
        $size = $param['size'];unset($param['size']);
        $data = $this->goodsCommentService->listPc($param,['nickname','create_time','goods_mark','goods_id','content','id','reply_content','purchase_time','member_image','image'], $page, $size);
        return success($data);
    }

    /**
     * Notes: 获取评论数量统计
     * @RequestMapping(path="rate", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/9/27 15:10
     * @return array
     * @throws BaseException
     */
    public function commentRate(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['goods_id' => 'required|integer|min:1'],
            ['goods_id.*' => 'goods_id参数传递错误']
        );
        $data = $this->goodsCommentService->goodsCommentRate($param['goods_id']);
        return success($data);
    }
}
