<?php

namespace App\Controller\Pc\Config;

use App\Common\InspectHelper;
use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Config\AreaService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 */
class AreaController extends BaseController
{
    /**
     * @Inject
     * @var AreaService
     */
    protected AreaService $areaService;

    /**
     * Notes: 获取下级地址
     * @RequestMapping(path="list", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/10/18 10:28
     * @return array
     * @throws BaseException
     */
    public function list(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'parent_id' => 'integer|min:0',
            ],
            ['parent_id.*' => 'parent_id参数错误']
        );

        return success($this->areaService->getRows(['p_id'=>$param['parent_id']], ['name','id','p_id']));
    }
}
