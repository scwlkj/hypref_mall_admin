<?php

namespace App\Controller\Pc\Config;

use App\Common\InspectHelper;
use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Config\ArticleService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 */
class ArticleController extends BaseController
{
    /**
     * @Inject
     * @var ArticleService
     */
    protected ArticleService $articleService;

    /**
     * Notes: 帮助中心首页
     * @RequestMapping(path="index", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/7/20 14:11
     * @return array
     * @throws BaseException
     */
    public function index():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1'
            ],
            ['page.*' => 'page参数错误','size.*' => 'size参数错误' ]
        );

        return success($this->articleService->index(['status'=>1], ['id','title','cover','description','create_time'], $param['page'], $param['size']));
    }

    /**
     * Notes: 文章详情
     * Author: Bruce.z
     * DateTime: 2022/7/20 14:14
     * @return array
     * @throws BaseException
     */
    public function detail():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'required|integer|min:1'],
            ['id.*' => 'page参数错误' ]
        );

        return success($this->articleService->getOne(['id'=>$param['id']],['id','title','cover','description','create_time','content']));
    }
}
