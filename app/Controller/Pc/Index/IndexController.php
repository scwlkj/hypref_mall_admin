<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Pc\Index;

use App\Cache\Cache;
use App\Common\DataConvert;
use App\Common\InspectHelper;
use App\Constants\BaseConstants;
use App\Constants\CacheKey;
use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Activity\CouponService;
use App\Service\Config\AdvService;
use App\Service\Config\HotSearchService;
use App\Service\Config\IndexMenuService;
use App\Service\Config\NotifyService;
use App\Service\Goods\CateService;
use App\Service\Goods\GoodsService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Utils\Context;

/**
 * @Controller()
 */
class IndexController extends BaseController
{
    /**
     * @Inject
     * @var AdvService
     */
    protected AdvService $advService;

    /**
     * @Inject
     * @var  GoodsService
     */
    protected GoodsService $goodsService;


    /**
     * @Inject
     * @var NotifyService
     */
    protected NotifyService $notifyService;

    /**
     * @Inject
     * @var CateService
     */
    protected CateService $cateService;

    /**
     * @Inject
     * @var IndexMenuService
     */
    protected IndexMenuService $indexMenuService;

    /**
     * @Inject
     * @var HotSearchService
     */
    protected HotSearchService $hotSearchService;

    /**
     * @Inject
     * @var CouponService
     */
    protected CouponService $couponService;

    /**
     * Notes: 获取首页信息
     * @RequestMapping(path="", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/6/13 13:46
     */
    public function index()
    {
        //$data = Cache::getInstance()->get(CacheKey::APP_INDEX);
        //if(empty($data)){
            // 获取轮播图、首页横幅活动
            $data['adv'] = $this->advService->indexAdv();

            // 获取菜单
            $data['menu'] = $this->indexMenuService->getRows(['status'=>BaseConstants::STATUS_YES], ['name','pc_link','link','icon']);

            // 获取 团购商品
            $data['group_goods'] = [];

            // 获取公告
            $data['gg'] = $this->notifyService->appIndexList();

            // 获取推荐商品
            $data['rec_goods'] = $this->goodsService->appIndexList();

            // 热搜词
            $data['hot_search'] = $this->hotSearchService->appOne();

            // 热搜词 列表
            $data['hot_search_list'] = $this->hotSearchService->appList();

            //Cache::getInstance()->set(CacheKey::APP_INDEX, $data, 5*60);
        //}

        return success($data);
    }

    /**
     * Notes: 分类列表
     * @RequestMapping(path="category", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/6/14 13:39
     * @return array
     */
    public function category(): array
    {
        $data = Cache::getInstance()->get(CacheKey::APP_CATEGORY);
        if(empty($data)){
            $data = $this->cateService->tree();
            Cache::getInstance()->set(CacheKey::APP_CATEGORY, $data);
        }
        return success($data);
    }

    /**
     * Notes: 公告详情
     * @RequestMapping(path="notifyDetail", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/6/14 14:40
     * @return mixed
     * @throws BaseException
     */
    public function notifyDetail()
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'integer|min:1'],
            ['id.*' => '参数传递错误']
        );
        return success($this->notifyService->detail($param['id']));
    }

    /**
     * Notes: c端热搜词列表
     * @RequestMapping (path="hotSearch", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/8/16 16:34
     * @return array
     */
    public function hotSearch(): array
    {
        $data = $this->hotSearchService->appList();
        return success($data);
    }

    /**
     * Notes: 优惠券列表
     * @RequestMapping (path="coupon", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/9/7 15:58
     * @return array
     * @throws BaseException
     */
    public function coupon():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'integer|required|min:1',
                'size' => 'integer|required|min:1',
            ],
            ['*.*' => '参数传递错误']
        );

        $data = $this->couponService->appIndex($param['page'], $param['size']);
        return success($data);
    }
}
