<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Pc\Index;

use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Activity\CouponReceiveService;
use App\Service\Activity\CouponService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcGetMemberMiddleware;

/**
 * Class CouponController
 * @package App\Controller\Pc\Index
 * @Controller()
 */
class CouponController extends BaseController
{
    /**
     * @Inject
     * @var CouponService
     */
    protected CouponService $couponService;

    /**
     * @Inject
     * @var InspectHelper
     */
    protected InspectHelper $inspectHelper;

    /**
     * @Inject
     * @var CouponReceiveService
     */
    protected CouponReceiveService $couponReceiveService;

    /**
     * Notes: 优惠券列表
     * @RequestMapping (path="", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/9/7 15:58
     * @return array
     * @throws BaseException
     */
    public function list():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'integer|required|min:1',
                'size' => 'integer|required|min:1',
            ],
            ['*.*' => '参数传递错误']
        );

        $data = $this->couponService->appIndex($param['page'], $param['size']);
        return success($data);
    }

    /**
     * Notes: 领券
     * @RequestMapping (path="", methods="post")
     * @Middlewares({
     *     @Middleware(\App\Middleware\PcGetMemberMiddleware::class)
     * })
     * Author: Bruce.z
     * DateTime: 2022/9/8 11:04
     * @return array
     * @throws BaseException
     */
    public function add():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'integer|required|min:1',
            ],
            ['id.*' => 'id参数传递错误']
        );

        $_data['id'] = $param['id'];
        $_data['member_id'] = get_user_info()['id'] ?? 0;

        $data = $this->couponReceiveService->add($_data);
        return success($data);
    }
}
