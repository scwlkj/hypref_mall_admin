<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

namespace App\Controller\Pc\Member;

use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Member\MemberService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 */
class AuthController extends BaseController
{
    /**
     * @Inject
     * @var MemberService
     */
    protected MemberService $memberService;

    /**
     * Notes: pc端注册
     * @RequestMapping(path="register", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/9/26 9:55
     * @return array
     * @throws BaseException
     */
    public function register(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'userName' => 'required|min:4|max:16',
                'password' => 'required|string|min:6',
            ],
            [
                'userName.*' => '用户名必须4-16位',
                'password.*' => '密码输入有误',
            ]
        );

        $data = $this->memberService->registerPc($param);
        return success($data);
    }

    /**
     * Notes: pc端 - 会员登录
     * @RequestMapping(path="login", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/9/26 10:34
     * @return array
     * @throws BaseException
     */
    public function login(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'userName' => 'required|min:4|max:16',
                'password' => 'required|string|min:6',
            ],
            [
                'userName.*' => '用户名必须4-16位',
                'password.*' => '密码输入有误',
            ]
        );

        $data = $this->memberService->login($param['userName'],$param['password']);
        return success($data);
    }
}
