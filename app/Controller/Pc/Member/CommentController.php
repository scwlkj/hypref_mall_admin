<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

namespace App\Controller\Pc\Member;

use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\CommentService;
use App\Service\Orders\GoodsCommentService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcAuthMiddleware;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(PcAuthMiddleware::class)
 * })
 */
class CommentController extends BaseController
{
    /**
     * @Inject
     * @var GoodsCommentService
     */
    protected GoodsCommentService $goodsCommentService;

    /**
     * @Inject
     * @var CommentService
     */
    protected CommentService $commentService;

    /**
     * Notes: 我的评论页
     * @RequestMapping(path="index", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/10/20 9:32
     * @return array
     * @throws BaseException
     */
    public function index(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['page' => 'required|integer|min:1','size' => 'required|integer'],
            ['*.*' => '参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        $page = $param['page'];
        $size = $param['size'];
        $member_id = get_user_info()['id'];
        $data = $this->goodsCommentService->listPc(['member_id'=>$member_id],['nickname','create_time','goods_mark','goods_id','content','id','reply_content','purchase_time','member_image','image','oid'], $page, $size);
        return success($data);
    }

    /**
     * Notes: 评论详情
     * @RequestMapping(path="detail", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/10/22 14:47
     * @return array
     * @throws BaseException
     */
    public function detail(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'required|integer|min:1'],
            ['*.*' => '参数错误' ]
        );

        $data = $this->commentService->commentDetail($param['id']);
        return success($data);
    }
}
