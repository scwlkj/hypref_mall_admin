<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

namespace App\Controller\Pc\Member;

use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Activity\CouponReceiveService;
use App\Service\Goods\GoodsCollectService;
use App\Service\Member\AddressService;
use App\Service\Member\MemberPointService;
use App\Service\Member\MemberService;
use App\Service\Member\PointLogService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcAuthMiddleware;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(PcAuthMiddleware::class)
 * })
 */
class MemberController extends BaseController
{
    /**
     * @Inject
     * @var MemberService
     */
    protected MemberService $memberService;

    /**
     * @Inject
     * @var AddressService
     */
    protected AddressService $addressService;

    /**
     * @Inject
     * @var GoodsCollectService
     */
    protected GoodsCollectService $goodsCollectService;

    /**
     * @Inject
     * @var PointLogService
     */
    protected PointLogService $pointLogService;

    /**
     * @Inject
     * @var MemberPointService
     */
    protected MemberPointService $memberPointService;

    /**
     * @Inject
     * @var CouponReceiveService
     */
    protected CouponReceiveService $couponReceiveService;

    /**
     * Notes: 退出登录
     * @RequestMapping(path="logout", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/9/26 15:23
     * @return array
     */
    public function logout(): array
    {
        $token = get_token();
        $data = $this->memberService->logout($token);
        return success($data);
    }

    /**
     * Notes: 添加收货地址
     * @RequestMapping(path="addAddress", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/18 10:54
     * @return array
     * @throws BaseException
     */
    public function addAddress(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'name' => 'string|required',
                'id' => 'integer',
                'mobile.*' => 'mobile|required',
                'regions' => 'string|required',
                'isDefault' => 'required|bool',
                'detailAddress' => 'required|string',
            ],
            ['*.*' => '参数错误']
        );
        $member_id = get_user_info()['id'] ?? 0;
        $data = $this->addressService->createByPc($member_id, $param);
        return success($data);
    }

    /**
     * Notes: 获取用户收货地址
     * @RequestMapping(path="addressList", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/10/18 11:14
     * @return array
     */
    public function addressList(): array
    {
        $member_id = get_user_info()['id'] ?? 0;
        $data = $this->addressService->getRows(['member_id'=>$member_id],['id','ship_name','ship_mobile','is_default','ship_addr','ship_province_id','ship_city_id','ship_county_id']);
        return success($data);
    }

    /**
     * Notes: 收货地址删除
     * @RequestMapping(path="addressDelete", methods="delete")
     * Author: Bruce.z
     * DateTime: 2022/10/18 11:45
     * @return array
     * @throws BaseException
     */
    public function addressDelete(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'integer|required',
            ],
            ['*.*' => '参数错误']
        );

        $data = $this->addressService->delete($param['id']);
        return success($data);
    }

    /**
     * Notes:获取地址详情
     * @RequestMapping(path="address", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/10/18 13:37
     */
    public function address(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'integer|required',
            ],
            ['*.*' => '参数错误']
        );

        $member_id = get_user_info()['id'] ?? 0;
        $data = $this->addressService->getOne(['id'=>$param['id'],'member_id'=>$member_id],['id','ship_name','ship_mobile','is_default','ship_addr','ship_province_id','ship_city_id','ship_county_id','member_id']);
        return success($data);
    }

    /**
     * Notes: 设置默认地址
     * @RequestMapping(path="setDefault", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/19 10:30
     * @return array
     * @throws BaseException
     */
    public function setDefault(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'integer|required',
            ],
            ['*.*' => '参数错误']
        );

        $member_id = get_user_info()['id'] ?? 0;
        $data = $this->addressService->setDefault($member_id, $param['id']);
        return success($data);
    }

    /**
     * Notes: 我的商品收藏列表
     * @RequestMapping(path="collect", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/10/18 18:03
     * @return array
     * @throws BaseException
     */
    public function collect():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
            ],
            [
                'page.*' => '参数传递错误',
                'size.*' => '参数传递错误',
            ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->goodsCollectService->memberCollect(get_user_info()['id'], $param['page'], $param['size']));
    }

    /**
     * Notes: 积分记录
     * @RequestMapping(path="pointLog", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/7/19 16:42
     * @return array
     * @throws BaseException
     */
    public function pointLog(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['page' => 'required|integer|min:1'],
            ['page.*' => 'page参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->pointLogService->index(['member_id'=>get_user_info()['id']],['type','point','remark','create_time'], $param['page'], 10));
    }

    /**
     * Notes: 会员积分
     * @RequestMapping(path="point", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/7/19 16:42
     * @return array
     * @throws BaseException
     */
    public function point(): array
    {
        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        return success($this->memberPointService->getOne(['member_id'=>get_user_info()['id']],['point','use_point','disable_point']));
    }

    /**
     * Notes: 修改密码
     * @RequestMapping(path="modifyPassword", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/19 20:16
     * @return array
     * @throws BaseException
     */
    public function modifyPassword(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'newPassword' => 'required|string|min:6',
                'oldPassword' => 'required|string|min:6',
            ],
            [
                'newPassword.*' => '密码不能少于6位',
                'oldPassword.*' => '密码输入有误',
            ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        $member_id = get_user_info()['id'];
        $data = $this->memberService->resetPassword($member_id, $param['oldPassword'], $param['newPassword']);
        return success($data);
    }

    /**
     * Notes: 更新个人资料
     * @RequestMapping(path="update", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/20 9:00
     * @return array
     * @throws BaseException
     */
    public function update():array
    {
        $params = array_only($this->request->all(),[
            'username','image','sex','birth'
        ]);
        $params = $this->inspectHelper->check(
            $params,
            [
                'username' => 'string|min:1',
                'image' => 'string|min:1',
                'sex' => 'integer|between:0,2',
                'birth' => 'date',
            ],
            [
                'username.*' => '昵称输入有误',
                'image.*' => '图片信息有误',
                'sex.*' => '未知性别',
                'birth.*' => '生日格式错误'
            ]
        );
        if (isset( $params['birth'])) {
            $params['birth'] = strtotime($params['birth']);
        }
        return success($this->memberService->update( get_user_info()['id'] ?? 0,$params));
    }

    /**
     * Notes:会员领取的优惠券列表
     * @RequestMapping(path="coupon", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/9/13 10:56
     * @return array
     * @throws BaseException
     */
    public function coupon(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
                'status' => 'required|integer|min:0',
            ],
            [
                'page.*' => 'page参数错误',
                'size.*' => 'size参数错误',
                'status.*' => 'status参数错误'
            ]
        );

        $_condition = [
            'member_id'=>get_user_info()['id'],
            'status'=>$param['status']
        ];

        $data = $this->couponReceiveService->memberCoupon($_condition,$param['page'], $param['size']);
        return success($data);
    }
}
