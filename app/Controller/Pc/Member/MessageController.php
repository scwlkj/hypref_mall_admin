<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

namespace App\Controller\Pc\Member;

use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Member\MessageService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcAuthMiddleware;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(PcAuthMiddleware::class)
 * })
 */
class MessageController extends BaseController
{

    /**
     * @Inject
     * @var MessageService
     */
    protected MessageService $messageService;

    /**
     * Notes: 站内信消息
     * @RequestMapping(path="index", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/7/20 11:14
     * @return array
     * @throws BaseException
     */
    public function message():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['page' => 'required|integer|min:1','status' => 'required|integer|min:0|max:1'],
            ['*.*' => '参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        return success($this->messageService->index(['member_id'=>get_user_info()['id'],'status'=>$param['status']],['title','status','oid','content','create_time','sender_type','id'], $param['page'], 10));
    }

    /**
     * Notes: 全部已读
     * @RequestMapping(path="read", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/7/20 13:31
     * @return array
     * @throws BaseException
     */
    public function messageRead(): array
    {
        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        return success($this->messageService->messageRead(get_user_info()['id']));
    }

    /**
     * Notes: 设置单条已读
     * @RequestMapping(path="readOne", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/19 13:58
     * @return array
     * @throws BaseException
     */
    public function readOne(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'required|integer|min:1'],
            ['id.*' => 'id参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        $data = $this->messageService->update($param['id'],['status'=>1]);
        return success($data);
    }

    /**
     * Notes: 删除单条消息
     * @RequestMapping(path="delete", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/19 13:59
     * @return array
     * @throws BaseException
     */
    public function delete(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'required|integer|min:1'],
            ['id.*' => 'id参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        $data = $this->messageService->delete($param['id']);
        return success($data);
    }
}
