<?php
/**
 * This file is part of Project.
 *
 * @link     https://www.xxx.com
 * @contact  xxxx@xxx.com
 */

namespace App\Controller\Pc\Member;

use App\Controller\Pc\BaseController;
use App\Exception\BaseException;
use App\Service\Goods\GoodsService;
use App\Service\Member\MemberFooterService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcAuthMiddleware;

/**
 * @Controller()
 * @Middlewares({
 *     @Middleware(PcAuthMiddleware::class)
 * })
 */
class FooterController extends BaseController
{
    /**
     * @Inject
     * @var MemberFooterService
     */
    protected MemberFooterService $memberFooterService;

    /**
     * @Inject
     * @var GoodsService
     */
    protected GoodsService $goodsService;

    /**
     * Notes: 添加和更新足迹
     * @RequestMapping(path="", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/10/24 10:26
     * @return array
     * @throws BaseException
     */
    public function add(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['goods_id' => 'required|integer|min:1'],
            ['*.*' => '参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);

        $member_id = get_user_info()['id'];
        $data = [
            'goods_id'=>$param['goods_id'],
            'member_id'=>$member_id,
        ];

        $data = $this->memberFooterService->add($data);
        return success($data);
    }

    /**
     * Notes: 删除单条足迹
     * @RequestMapping(path="", methods="delete")
     * Author: Bruce.z
     * DateTime: 2022/10/24 10:28
     * @return array
     * @throws BaseException
     */
    public function delete(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['id' => 'required|integer|min:1'],
            ['*.*' => '参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        $data = $this->memberFooterService->delete($param['id']);

        return success($data);
    }

    /**
     * Notes: 足迹列表
     * @RequestMapping(path="", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/10/24 10:40
     * @return array
     * @throws BaseException
     */
    public function index(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['page' => 'required|integer|min:1','size' => 'required|integer|min:1'],
            ['*.*' => '参数错误' ]
        );

        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        $member_id = get_user_info()['id'];
        $data = $this->memberFooterService->index(['member_id'=>$member_id],['id','goods_id','create_time'], $param['page'], $param['size']);

        if(!empty($data)){
            $goods_ids = array_column($data['data'],'goods_id');
            $goods = $this->goodsService->getRows(['id'=>$goods_ids], ['id','img','name','retail_price']);
            $goods = array_column($goods,null,'id');

            foreach ($data['data'] as &$re){
                $re['goods_name'] = $goods[$re['goods_id']]['name']??'';
                $re['goods_img'] = $goods[$re['goods_id']]['img']??'';
                $re['goods_price'] = $goods[$re['goods_id']]['retail_price']??'';
            }
        }

        return success($data);
    }

    /**
     * Notes: 清空记录
     * @RequestMapping(path="clear", methods="delete")
     * Author: Bruce.z
     * DateTime: 2022/10/24 11:27
     * @return array
     * @throws BaseException
     */
    public function clear(): array
    {
        if(!get_user_info()) throw new BaseException(BaseException::TOKEN_ERROR);
        $member_id = get_user_info()['id'];
        $data = $this->memberFooterService->removeAll(['member_id'=>$member_id]);
        return success($data);
    }
}
