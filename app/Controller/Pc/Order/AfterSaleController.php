<?php

namespace App\Controller\Pc\Order;


use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\AfterSaleService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcAuthMiddleware;

/**
 * @Middlewares({
 *     @Middleware(PcAuthMiddleware::class)
 * })
 * @Controller()
 */
class AfterSaleController extends BaseController
{
    /**
     * @Inject
     * @var AfterSaleService
     */
    protected AfterSaleService $afterSaleService;

    /**
     * Notes:申请订单售后
     * @RequestMapping(path="apply", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/18 15:50
     * @return array
     * @throws BaseException
     */
    public function apply(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'oid' => 'required',
                'order_detail_id' => 'required',
                'return_money' => 'required',
                'goods_id' => 'required',
                'product_id' => 'required',
                'reason' => 'required',
                'description' => 'required',
                'image' => 'required',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $param['member_id'] = get_user_info()['id'];
        $data = $this->afterSaleService->add($param);
        return success($data);
    }

    /**
     * Notes: 我的售后列表
     * @RequestMapping (path="list",methods="get")
     * Author: Bruce.z
     * DateTime: 2022/8/19 9:49
     * @return array
     * @throws BaseException
     */
    public function list(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required',
                'size' => 'required',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $param['member_id'] = get_user_info()['id'];
        $data = $this->afterSaleService->appList($param);
        return success($data);
    }

    /**
     * Notes: 订单详情
     * @RequestMapping(path="detail", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/8/19 10:24
     * @return array
     * @throws BaseException
     */
    public function detail(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'number' => 'required',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $data = $this->afterSaleService->appDetail($param['number']);
        return success($data);
    }

    /**
     * Notes:
     * @RequestMapping(path="applyPre", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/10/29 11:37
     * @return array
     * @throws BaseException
     */
    public function applyPre(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $data = $this->afterSaleService->applyPre($param['id']);
        return success($data);
    }

}
