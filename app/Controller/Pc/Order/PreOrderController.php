<?php

namespace App\Controller\Pc\Order;

use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\PreOrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcAuthMiddleware;

/**
 * @Middlewares({
 *     @Middleware(PcAuthMiddleware::class)
 * })
 * @Controller()
 */
class PreOrderController extends BaseController
{
    /**
     * @Inject
     * @var PreOrderService
     */
    protected $preOrderService;

    /**
     * Notes: 预提交订单
     * @RequestMapping(path="initData", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/7/9 17:07
     * @return array
     * @throws BaseException
     */
    public function initData(): array
    {
        $param = $this->inspectHelper->check(
            array_only($this->request->all(), ['coupon_receive_id','address_id']),
            [
                'coupon_receive_id' => 'integer|min:0',
                'address_id' => 'integer|min:0',
            ],
            [
                'coupon_receive_id.*' => 'coupon_receive_id参数传递错误',
                'address_id.*' => 'address_id参数传递错误',
            ]
        );
        $data = $this->preOrderService->initData([], get_user_info()['id'] ?? 0, $param['coupon_receive_id']??0,$param['address_id']??0);
        return success($data);
    }

    /**
     * Notes: 立即购买
     * Author: Bruce.z
     * @return array
     * @throws BaseException
     */
    public function initBuy(): array
    {
        $param = array_only($this->request->all(), ['goods_id','product_id','goods_number']);
        $param = $this->inspectHelper->check(
            $param,
            [
                'goods_id' => 'integer|required|min:1',
                'product_id' => 'integer|required|min:1',
                'goods_number' => 'integer|required|min:1'
            ],
            [
                'goods_id.*' => 'goods_id参数传递错误',
                'product_id.*' => 'goods_id参数传递错误',
                'goods_number.*' => 'goods_id参数传递错误',
            ]
        );

        $data = $this->preOrderService->initBuy($param, get_user_info()['id'] ?? 0);
        return success($data);
    }
}
