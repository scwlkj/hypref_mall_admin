<?php

namespace App\Controller\Pc\Order;


use App\Common\InspectHelper;
use App\Controller\App\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\CommentService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\Annotation\Middleware;
use App\Middleware\PcAuthMiddleware;

/**
 * @Middlewares({
 *     @Middleware(PcAuthMiddleware::class)
 * })
 * @Controller()
 */
class CommentController extends BaseController
{

    /**
     * @Inject
     * @var CommentService
     */
    protected CommentService $commentService;

    /**
     * Notes: 添加评论
     * @RequestMapping(path="add", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/17 20:22
     * @return array
     * @throws BaseException
     */
    public function add(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'oid' => 'required',
                'formData'=>'required|array',
                'logistics'=>'required',
                'service'=>'required',
            ],
            ['*.required' => ':attribute 为必填字段' ]
        );

        $param['member_id'] = get_user_info()['id'];
        $re = $this->commentService->add($param);
        return success($re);
    }

}
