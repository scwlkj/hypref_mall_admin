<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Member;


use App\Controller\Admin\BaseController;
use App\Service\Member\LevelsService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class LevelsController extends BaseController
{
    /**
     * @Inject
     * @var LevelsService
     */
    protected $service;
}
