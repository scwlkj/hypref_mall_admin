<?php

namespace App\Controller\Admin\Member;

use App\Controller\Admin\BaseController;
use App\Service\Member\AddressService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class AddressController extends BaseController
{
    /**
     * @Inject
     * @var AddressService
     */
    protected $service;
}
