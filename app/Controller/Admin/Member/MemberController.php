<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Member;


use App\Controller\Admin\BaseController;
use App\Service\Member\MemberService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class MemberController extends BaseController
{
    /**
     * @Inject
     * @var MemberService
     */
    protected $service;
}
