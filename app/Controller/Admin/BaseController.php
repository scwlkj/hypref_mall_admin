<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin;


use App\Common\InspectHelper;
use App\Controller\AbstractController;
use App\Service\BaseService;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Di\Annotation\Inject;

class BaseController extends AbstractController
{
    const SUCCESS_CODE = 200;
    const ERROR_CODE = 0;
    const SUCCESS_MSG = 'success';

    protected $service;

    /**
     * @Inject
     * @var InspectHelper
     */
    protected InspectHelper $inspectHelper;

    /**
     * @RequestMapping(path="", methods="get")
     * Author: Bruce.z
     * DateTime: 2021/12/28 11:06
     * @return mixed
     */
    public function index()
    {
        $param = $this->request->all();
        $page = $param['page'] ?? 1;unset( $param['page']);
        $size = $param['size'] ?? 1;unset( $param['size']);

        $condition = $param ?? [];
        return success($this->service->index($condition, ['*'], $page, $size));
    }

    /**
     * @RequestMapping(path="", methods="post")
     * Author: Bruce.z
     * DateTime: 2021/12/28 11:06
     * @return mixed
     */
    public function add()
    {
        $param = $this->request->all();
        return success($this->service->add($param));
    }

    /**
     * @RequestMapping(path="{id:\d+}", methods="post")
     * Author: Bruce.z
     * @param int $id
     * @return mixed
     */
    public function update(int $id)
    {
        $param = $this->request->all();
        return success($this->service->update($id, $param));
    }

    /**
     * @RequestMapping(path="{id:\d+}", methods="delete")
     * Author: Bruce.z
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return success($this->service->delete($id));
    }

    /**
     * @RequestMapping(path="{id:\d+}", methods="get")
     * Author: Bruce.z
     * @param int $id
     * @return mixed
     */
    public function detail(int $id)
    {
        return success($this->service->detail($id));
    }

}
