<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Config;


use App\Controller\Admin\BaseController;
use App\Service\Config\ArticleService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class ArticleController extends BaseController
{
    /**
     * @Inject
     * @var ArticleService
     */
    protected $service;
}
