<?php

namespace App\Controller\Admin\Config;

use App\Controller\Admin\BaseController;
use App\Service\Config\HotSearchService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class HotSearchController extends BaseController
{
    /**
     * @Inject
     * @var HotSearchService
     */
    protected $service;
}
