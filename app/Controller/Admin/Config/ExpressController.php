<?php

namespace App\Controller\Admin\Config;

use App\Constants\BaseConstants;
use App\Controller\Admin\BaseController;
use App\Service\Config\ExpressService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 */
class ExpressController extends BaseController
{
    /**
     * @Inject
     * @var ExpressService
     */
    protected $service;

    /**
     * Notes: 物流公司列表 - select 框
     * @RequestMapping(path="list", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/8/12 16:03
     * @return array
     */
    public function selectList(): array
    {
        $list = $this->service->getRows(['status'=>BaseConstants::STATUS_YES],['id','code','name']);
        return success($list);
    }
}
