<?php

namespace App\Controller\Admin\Config;

use App\Controller\Admin\BaseController;
use App\Service\Config\NotifyService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class NotifyController extends BaseController
{
    /**
     * @Inject
     * @var NotifyService
     */
    protected $service;
}
