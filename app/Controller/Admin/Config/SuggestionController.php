<?php

namespace App\Controller\Admin\Config;

use App\Controller\Admin\BaseController;
use App\Service\Config\SuggestionService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class SuggestionController extends BaseController
{
    /**
     * @Inject
     * @var SuggestionService
     */
    protected $service;
}
