<?php

namespace App\Controller\Admin\Config;

use App\Controller\Admin\BaseController;
use App\Service\Config\IndexMenuService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class IndexMenuController extends BaseController
{
    /**
     * @Inject
     * @var IndexMenuService
     */
    protected $service;
}
