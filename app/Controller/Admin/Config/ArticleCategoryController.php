<?php

namespace App\Controller\Admin\Config;

use App\Controller\Admin\BaseController;
use App\Service\Config\ArticleCategoryService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class ArticleCategoryController extends BaseController
{
    /**
     * @Inject
     * @var ArticleCategoryService
     */
    protected $service;

    /**
     * @RequestMapping(path="selectTree", methods="get")
     * Author: Bruce.z
     * @return mixed
     */
    public function selectTree()
    {
        return success($this->service->selectTree());
    }

    /**
     * @RequestMapping(path="tree", methods="get")
     * Author: Bruce.z
     * @return mixed
     */
    public function tree()
    {
        return success($this->service->tree());
    }
}
