<?php

namespace App\Controller\Admin\Config;

use App\Controller\Admin\BaseController;
use App\Service\Config\ConfigService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class ConfigController extends BaseController
{
    /**
     * @Inject
     * @var ConfigService
     */
    protected $service;


    /**
     * @RequestMapping(path="{key:\w+}", methods="get")
     * Notes:
     * Author: Bruce.z
     * DateTime: 2022/6/10 17:21
     * @param string $key
     * @return array
     */
    public function get(string $key): array
    {
        return success($this->service->get($key));
    }

    /**
     * @RequestMapping(path="set", methods="post")
     * Notes:
     * Author: Bruce.z
     * DateTime: 2022/6/10 17:21
     * @return array
     */
    public function set(): array
    {
        $param = $this->request->all();
        return success($this->service->set($param['key'], $param['data']));
    }
}
