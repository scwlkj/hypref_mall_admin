<?php

namespace App\Controller\Admin\Config;

use App\Controller\Admin\BaseController;
use App\Service\Config\AdvService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class AdvController extends BaseController
{
    /**
     * @Inject
     * @var AdvService
     */
    protected $service;

    /**
     * @RequestMapping(path="position", methods="get")
     * Notes:
     * Author: Bruce.z
     * DateTime: 2022/6/10 13:49
     * @return mixed
     */
    public function position()
    {
        $param = $this->request->all();
        return success($this->service->position($param));
    }
}
