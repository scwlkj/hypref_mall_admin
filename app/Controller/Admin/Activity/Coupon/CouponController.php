<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Activity\Coupon;

use App\Common\InspectHelper;
use App\Controller\Admin\BaseController;
use App\Exception\BaseException;
use App\Service\Activity\CouponReceiveService;
use App\Service\Activity\CouponService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;

/**
 * @Controller()
 */
class CouponController extends BaseController
{

    /**
     * @Inject
     * @var CouponService
     */
    protected $service;

    /**
     * @Inject
     * @var CouponReceiveService
     */
    protected CouponReceiveService $couponReceiveService;

    /**
     * @Inject
     * @var InspectHelper
     */
    protected InspectHelper $inspectHelper;


    /**
     * Notes: 单个活动更新状态
     * @RequestMapping(path="updateStatus", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/9/15 13:41
     * @return array
     * @throws BaseException
     */
    public function updateStatus(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
                'status' => 'required|integer',
            ],
            [
                'id.*' => 'id参数传递错误',
                'status.*' => 'status参数传递错误',
            ]
        );

        $data = $this->service->updateStatus([$param['id']], $param['status']);
        return success($data);
    }

    /**
     * Notes: 活动 发放/领取列表
     * @RequestMapping(path="receiveList", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/9/15 14:11
     * @return array
     * @throws BaseException
     */
    public function receiveList(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
                'coupon_id' => 'required|integer|min:1',
            ],
            [
                'page.*' => 'page参数传递错误',
                'size.*' => 'size参数传递错误',
                'coupon_id.*' => 'coupon_id参数传递错误',
            ]
        );

        $data = $this->couponReceiveService->getList($param['coupon_id'], $param['page'], $param['size']);
        return success($data);
    }

    /**
     * Notes: 添加 活动
     * @RequestMapping(path="create", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/9/15 17:40
     * @return array
     * @throws BaseException
     */
    public function create():array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'name' => 'required|string|min:1|max:50',
                'coupon_price' => 'required',
                'price' => 'required|min:0.01',
                'num' => 'required|min:1',
                'time2' => 'required|array',
            ],
            [
                '*.*' => '参数异常',
            ]
        );

        if($param['time_type'] == 0){
            if(!count($param['time1'])){
                throw new BaseException(BaseException::BASE_ERROR, '使用时间不能为空');
            }
        }else{
            if((int) $param['receive_after_day'] < 1){
                throw new BaseException(BaseException::BASE_ERROR, '领取后天数不能为空');
            }
        }

        if($param['goods_type'] == 3){
            if(!count($param['cateTree'])){
                throw new BaseException(BaseException::BASE_ERROR, '请选择指定分类');
            }
        }

        $re = $this->service->getOne(['name'=>$param['name']],['id']);
        if($re){
            throw new BaseException(BaseException::BASE_ERROR, '活动名称已存在！');
        }

        $param['start_time'] = isset($param['time2'][0])?($param['time2'][0]/1000):0;
        $param['end_time'] = $param['time2'][1]?($param['time2'][1]/1000):0;
        $param['receive_start_time'] = isset($param['time1'][0])?($param['time1'][0]/1000):0;
        $param['receive_end_time'] = isset($param['time1'][1])?($param['time1'][1]/1000):0;

        $data = $this->service->add($param);
        return success($data);
    }
}
