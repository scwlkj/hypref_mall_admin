<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Goods;


use App\Controller\AbstractController;
use App\Controller\Admin\BaseController;
use App\Service\Goods\TagService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class TagController extends BaseController
{
    /**
     * @Inject
     * @var TagService
     */
    protected $service;
}
