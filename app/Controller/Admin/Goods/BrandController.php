<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Goods;


use App\Controller\AbstractController;
use App\Controller\Admin\BaseController;
use App\Service\Goods\BrandService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class BrandController extends BaseController
{

    /**
     * @Inject
     * @var BrandService
     */
    protected $service;
}
