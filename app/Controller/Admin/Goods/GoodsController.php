<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Goods;


use App\Controller\Admin\BaseController;
use App\Service\Goods\GoodsService;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class GoodsController extends BaseController
{

    /**
     * @Inject
     * @var GoodsService
     */
    protected $service;

    /**
     * @RequestMapping(path="init", methods="get")
     * Author: Bruce.z
     * @return array
     */
   public function initData(): array
   {
       return success($this->service->initData());
   }
}
