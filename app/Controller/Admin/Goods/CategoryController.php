<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Goods;


use App\Controller\AbstractController;
use App\Controller\Admin\BaseController;
use App\Service\Goods\CateService;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\Di\Annotation\Inject;

/**
 * @Controller()
 */
class CategoryController extends BaseController
{
    /**
     * @Inject
     * @var CateService
     */
    protected $service;

    /**
     * @RequestMapping(path="tree", methods="get")
     * Author: Bruce.z
     * @return mixed
     */
    public function tree()
    {
        return success($this->service->tree());
    }

    /**
     * @RequestMapping(path="selectTree", methods="get")
     * Author: Bruce.z
     * @return mixed
     */
    public function selectTree()
    {
        return success($this->service->selectTree());
    }
}
