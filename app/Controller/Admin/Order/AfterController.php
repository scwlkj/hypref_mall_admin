<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Order;

use App\Common\InspectHelper;
use App\Controller\Admin\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\AfterSaleService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 */
class AfterController extends BaseController
{
    /**
     * @Inject
     * @var AfterSaleService
     */
    protected $service;

    /**
     * Notes:售后订单列表
     * @RequestMapping(path="list", methods="get")
     * @return array
     * @throws BaseException
     */
    public function list(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
                'oid' => 'string',
                'number' => 'string',
            ],
            [
                'page.*' => 'page参数传递错误',
                'size.*' => 'size参数传递错误',
            ]
        );

        $data = $this->service->list($param);
        return success($data);
    }

    /**
     * Notes: 拒绝用户售后申请
     * @RequestMapping(path="refuse", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/22 9:49
     * @return array
     * @throws BaseException
     */
    public function refuse(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'reason' => 'required|string',
                'number' => 'required|string',
            ],
            [
                'reason.*' => 'reason参数传递错误',
                'number.*' => 'number参数传递错误',
            ]
        );

        $data = $this->service->refuse($param['number'], $param['reason']);
        return success($data);
    }

    /**
     * Notes: 拒绝用户售后申请
     * @RequestMapping(path="agree", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/22 9:49
     * @return array
     * @throws BaseException
     */
    public function agree(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['number' => 'required|string'],
            ['number.*' => 'number参数传递错误']
        );

        $data = $this->service->agree($param['number']);
        return success($data);
    }

    /**
     * Notes: 售后单详情
     * @RequestMapping(path="info", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/8/22 17:38
     * @return array
     * @throws BaseException
     */
    public function info(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'number' => 'required',
            ],
            ['*.required' => ':attribute 字段缺失']
        );

        $data = $this->service->info($param['number']);
        return success($data);
    }
}
