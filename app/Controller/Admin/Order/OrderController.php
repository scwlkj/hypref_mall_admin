<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Order;

use App\Common\InspectHelper;
use App\Controller\Admin\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\OrderService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 */
class OrderController extends BaseController
{
    /**
     * @Inject
     * @var OrderService
     */
    protected $service;

    /**
     * @RequestMapping(path="", methods="get")
     * @throws BaseException
     */
    public function index()
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
            ],
            [
                'page.*' => 'page参数传递错误',
                'size.*' => 'size参数传递错误',
            ]
        );
        $page = $param['page'];unset($param['page']);
        $size = $param['size'];unset($param['size']);
        $data = $this->service->list($param, $page, $size);
        return success($data);
    }

    /**
     * Notes: A端订单详情
     * @RequestMapping (path="info",methods="get")
     * Author: Bruce.z
     * DateTime: 2022/8/10 15:30
     * @return array
     * @throws BaseException
     */
    public function info(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'oid' => 'required',
            ],
            [
                'oid.*' => 'oid参数传递错误',
            ]
        );
        $data = $this->service->orderDetail($param['oid']);
        return success($data);
    }

    /**
     * Notes: 取消订单
     * @RequestMapping (path="cancel",methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/11 15:19
     * @return array
     * @throws BaseException
     */
    public function cancel(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'oid' => 'required',
            ],
            [
                'oid.*' => 'oid参数传递错误',
            ]
        );
        $data = $this->service->cancel($param['oid']);
        return success($data);
    }

    /**
     * Notes: 修改为已支付
     * @RequestMapping (path="payed",methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/11 15:27
     * @return array
     * @throws BaseException
     */
    public function payed(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'oid' => 'required',
            ],
            [
                'oid.*' => 'oid参数传递错误',
            ]
        );
        $data = $this->service->payed($param['oid']);
        return success($data);
    }

    /**
     * Note:发货
     * @PostMapping(path="delivery")
     * Author:Jurij.cao
     * Date:2022/8/11
     * @return array
     * @return array
     * @throws BaseException
     */
    public function delivery(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['oid' => 'required','logi_no' => 'required' , 'logi_code' => 'required'],
            ['oid.*' => 'oid参数错误' , 'logi_no.*' => 'logi_no参数错误','logi_code.*' => 'logi_code参数错误']
        );

        return success($this->service->delivery($param));
    }

    /**
     * Note:确认收货
     * @PostMapping(path="confirm_goods")
     * Author:Jurij.cao
     * Date:2022/8/11
     * @return array
     * @return array
     * @throws BaseException
     */
    public function confirmGoods(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            ['oid' => 'required'],
            ['oid.*' => 'oid参数错误' ]
        );

        return success($this->service->confirmGoods($param['oid']));
    }
}
