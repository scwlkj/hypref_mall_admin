<?php
/**
 * This file is part of Hyperf Mall.
 *
 * @link     https://gitee.com/scwlkj/hyperf_mall
 * @contact  hyperf_mall@163.com
 */

namespace App\Controller\Admin\Order;

use App\Common\InspectHelper;
use App\Constants\BaseConstants;
use App\Controller\Admin\BaseController;
use App\Exception\BaseException;
use App\Service\Orders\CommentService;
use App\Service\Orders\GoodsCommentService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Annotation\Controller;

/**
 * @Controller()
 */
class CommentController extends BaseController
{
    /**
     * @Inject
     * @var GoodsCommentService
     */
    protected $service;

    /**
     * @Inject
     * @var CommentService
     */
    protected CommentService $commentService;

    /**
     * Notes:商品评论列表
     * @RequestMapping(path="list", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/8/19 14:39
     * @return array
     * @throws BaseException
     */
    public function list(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
                'oid' => 'string',
                'status' => 'integer',
                'goods_mark' => 'integer',
            ],
            [
                'page.*' => 'page参数传递错误',
                'size.*' => 'size参数传递错误',
            ]
        );
        $page = $param['page'];unset($param['page']);
        $size = $param['size'];unset($param['size']);
        $data = $this->service->list($param,['nickname','create_time','goods_mark','status','goods_id','content','oid','id','reply_content','purchase_time'], $page, $size);
        return success($data);
    }

    /**
     * Notes: 通过审核
     * @RequestMapping(path="pass", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/19 16:19
     * @return array
     * @throws BaseException
     */
    public function pass(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
            ],
            [
                'id.*' => 'id参数传递错误',
            ]
        );

        $data = $this->service->update($param['id'], ['status'=>BaseConstants::STATUS_YES]);
        return success($data);
    }

    /**
     * Notes: 驳回
     * @RequestMapping(path="unPass", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/19 16:19
     * @return array
     * @throws BaseException
     */
    public function unPass(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
            ],
            [
                'id.*' => 'id参数传递错误',
            ]
        );

        $data = $this->service->update($param['id'], ['status'=>BaseConstants::STATUS_MIX]);
        return success($data);
    }

    /**
     * Notes: 回复
     * @RequestMapping(path="replay", methods="post")
     * Author: Bruce.z
     * DateTime: 2022/8/19 16:19
     * @return array
     * @throws BaseException
     */
    public function replay(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'id' => 'required|integer|min:1',
                'content' => 'required|string',
            ],
            [
                'id.*' => 'id参数传递错误',
                'content.*' => 'content参数传递错误',
            ]
        );

        $data = $this->service->update($param['id'], ['reply_content'=>$param['content']]);
        return success($data);
    }

    /**
     * Notes:订单评论列表
     * @RequestMapping(path="orderList", methods="get")
     * Author: Bruce.z
     * DateTime: 2022/8/19 14:39
     * @return array
     * @throws BaseException
     */
    public function orderList(): array
    {
        $param = $this->inspectHelper->check(
            $this->request->all(),
            [
                'page' => 'required|integer|min:1',
                'size' => 'required|integer|min:1',
                'oid' => 'string',
            ],
            [
                'page.*' => 'page参数传递错误',
                'size.*' => 'size参数传递错误',
            ]
        );
        $page = $param['page'];unset($param['page']);
        $size = $param['size'];unset($param['size']);
        $data = $this->commentService->index($param,['nickname','delivery_speed_mark','delivery_service_mark','is_auto','create_time','oid','purchase_time'], $page, $size);
        return success($data);
    }

}
