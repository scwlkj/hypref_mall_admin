<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class CacheKey extends AbstractConstants
{
    /**
     * @Message("C端首页")
     */
    public const APP_INDEX = "app:index";

    /**
     * @Message("分类页面")
     */
    public const APP_CATEGORY = "app:category";
}
