<?php
declare(strict_types=1);

namespace App\Cache;

use App\Common\DataConvert;
use App\Common\Singleton;
use Hyperf\Redis\Redis;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class Cache
{
    use Singleton;

    public $redis;

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct()
    {
        $container = ApplicationContext::getContainer();
        $this->redis = $container->get(Redis::class);
    }

    public function get($key)
    {
        $data = $this->redis->get($key);
        return !$data ? null : DataConvert::decode($data);
    }

    public function exists($key)
    {
        return $this->redis->exists($key);
    }

    public function set($key, $data, $seconds = 0): bool
    {
        $data = DataConvert::encode($data);
        if (empty($seconds)) {
            return $this->redis->set($key, $data);
        }
        return $this->redis->set($key, $data, $seconds);
    }

    public function setNX($key, $data): bool
    {
        $data = DataConvert::encode($data);
        return $this->redis->setnx($key, $data);
    }

    public function expire($key, $seconds = 0): bool
    {
        return $this->redis->expire($key, $seconds);
    }

    public function del($key): int
    {
        return $this->redis->del($key);
    }

    public function hGet($key, $field)
    {
        return $this->redis->hget($key, $field);
    }

    public function hSet($key, $field, $data)
    {
        return $this->redis->hset($key, $field, $data);
    }

    public function hDel($key, $field)
    {
        return $this->redis->hdel($key, $field);
    }

    public function increment($key, $value = 1): int
    {
        return $this->redis->incrBy($key, $value);
    }

    public function ttl($key)
    {
        return $this->redis->ttl($key);
    }
}
