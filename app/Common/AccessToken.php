<?php
declare(strict_types=1);
/**
 * Notes: 存储Token信息
 */

namespace App\Common;

class AccessToken
{
    use Singleton;

    /**
     * @var string $token
     */
    private string $token;

    /**
     * @param string $token
     * @return string
     */
    public function setToken(string $token):string
    {
        return $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token ?? '';
    }
}
