<?php
declare(strict_types=1);

namespace App\Common;

class CurrentDb
{
    use Singleton;

    private $db;

    public function setDb($db)
    {
        $this->db = $db;
    }

    public function getDb(): string
    {
        return $this->db ?: '';
    }
}