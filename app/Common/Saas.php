<?php
declare(strict_types=1);
/**
 * Notes: 存储saas_id信息
 */

namespace App\Common;

class Saas
{
    use Singleton;

    private $id;

    /**
     * Notes:设置id
     * @param $id int
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Notes:获取id
     * @return int
     */
    public function getId(): int
    {
        return $this->id ?: 0;
    }
}
