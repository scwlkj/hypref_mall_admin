<?php
declare(strict_types=1);

namespace App\Common;

class StartTime
{
    use Singleton;

    private $time;

    public function setTime($time)
    {
        $this->time = $time;
    }

    public function getTime(): float
    {
        return $this->time ?: 0.00;
    }
}