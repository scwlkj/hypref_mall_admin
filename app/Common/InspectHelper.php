<?php
declare(strict_types=1);

namespace App\Common;

use App\Exception\BaseException;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;

class InspectHelper
{
    use Singleton;

    /**
     * @Inject
     * @var ValidatorFactoryInterface
     */
    protected $validationFactory;

    /**
     * @param array $params
     * @param array $rules
     * @param array $messages
     * @return array
     * @throws BaseException
     */
    public function check(array $params,array $rules, array $messages): array
    {
        $validator = $this->validationFactory->make($params, $rules, $messages);
        // 抛出第一个校验错误
        if ($validator->fails()){
            throw new BaseException(BaseException::BASE_ERROR, $validator->errors()->first());
        }
        // 过滤没有校验的参数
        //return array_intersect_key($params,$rules);

        return $params;
    }
}
