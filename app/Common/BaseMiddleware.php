<?php
declare(strict_types=1);


namespace App\Common;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface as HttpResponse;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class BaseMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var HttpResponse
     */
    protected $response;

    public function __construct(ContainerInterface $container, HttpResponse $response, RequestInterface $request)
    {
        $this->container = $container;
        $this->response = $response;
        $this->request = $request;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        //$businessId = $request->getHeader('SAAS_ID')[0] ?? 0;
        $requestId = $request->getHeader('x-request-id')[0] ?? uniqid();
        $clientIP = $request->getHeader('x-real-ip')[0] ?? '0.0.0.0';

        $common_info = [
            'request_id' => $requestId,
            'client_ip'  => $clientIP
        ];

        //set_saas_id($businessId);
        //set_merchant_id((int)$businessId);
        ///set_super_saas_id((int)$superSaasId);
       // set_common_info($common_info);

        return $handler->handle($request);
    }
}
