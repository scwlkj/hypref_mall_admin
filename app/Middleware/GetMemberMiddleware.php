<?php
declare(strict_types=1);
/**
 * 仅获取会员信息 获取不到 不报错
 */

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class GetMemberMiddleware extends AppAuthMiddleware
{
    /**
     * Notes: 获取会员有就获取，没有也不报错
     * Author: Bruce.z
     * DateTime: 2022/7/18 9:16
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return parent::process($request, $handler);
        } catch (\Exception $exception) {
            return $handler->handle($request);
        }
    }
}
